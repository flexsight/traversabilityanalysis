
# Visual-Geometric Traversability Analysis (VGTA+)

This is the official repository for the paper titled "To Drive or Not To Drive: A Hybrid Visual-Geometric Approach for Traversability Analysis in Autonomus Driving Applications".

![Alt text](img/gt.png)

![Alt text](img/trav.png)



## Requirements:
 - Install [ROS](http://www.ros.org/install/)
[comment]: <> ( - Install [LeGO-LOAM]&#40;https://github.com/RobustFieldAutonomyLab/LeGO-LOAM&#41;.)
 - PCL ( tested on [PCL 1.8](https://pcl.readthedocs.io/projects/tutorials/en/latest/compiling_pcl_posix.html))
 - dataset ( bag of scenario00 of SemanticKitti [1] ): https://drive.google.com/file/d/1s-3hQ7j00fJyRCyAowuHx1Z-fih0-ekL/view?usp=sharing

### IMPORTANT:
 - Modify paths and parameters directly in the config/params.yaml file!
    - N.B. In ROS, relative paths are based on the hidden directory "/.ros"
    - For example, cut and paste the files */rbf_svm.yaml* and */norm_configs.txt* to the folder */.ros/vgta+/*, in order to get it work with the default parameters set in "traversabilityanalysis/src/TRAV_ANALYSIS/config/params.yaml". Otherwise, modify such paths.
      - to do so, run
    

         mkdir -p ~/.ros/vgta+/
         cp rbf_svm.yaml /.ros/vgta+/rbf_svm.yaml
         cp norm_configs.txt /.ros/vgta+/norm_configs.txt

####
      # The final folder structure should be

      .                                   # root directory
      ├── ...
      ├── .ros/
      │   ├── vgta+/
      │   │   ├── norm_params.txt         # parametersfor normalize the new data
      │   │   └── rbf_svm.yml             # rbf model's weights
      └── ...

***
## Compile:
    source /opt/ros/noetic/setup.bash
    catkin build -j4
***

# How-To run the code


### Analyze ( best results when using the ground truth odometry )
### Terminal 1
    source /opt/ros/noetic/setup.bash
    roslaunch trav_analysis analyzer.launch frame:=velodyne
### Terminal 2
    source /opt/ros/noetic/setup.bash
    rosbag play sk00.bag --clock -r 0.5
***


## Produce/Train/Analyze
### Terminal 1
    source /opt/ros/noetic/setup.bash
    source devel/setup.bash
    
    /// IF you want to produce Features:
    roslaunch trav_analysis produceFeature.launch
    
    /// IF you want to train a model (no need of terminal 2):
    roslaunch trav_analysis trainModel.launch

    /// IF you want to run the trained system on a recorded bag:
    roslaunch trav_analysis analyzer.launch frame:=velodyne

### Terminal 2
    source /opt/ros/noetic/setup.bash
    rosbag play semantickitti_sequence00.bag --clock -r 0.3



## References
[SemanticKitti](http://www.semantic-kitti.org/)



# TO ADD NEW FEATURES (in the code)
 - create var in features.h
 - init to zero in features.cpp
 - calculate in computefeatures
 - write to file in produce features (clearly, pre label)
 - increment total line length in predicter.cpp (in init, loop where check if colskip)