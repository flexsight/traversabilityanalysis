#include "utility.h"

class ProduceFeatures : public ParamServer
{
    ros::Subscriber subVelodyneCloud;
    ros::Subscriber subImageLeft;

    ros::Publisher pubTraversabilityGridCentersAsCloud;
    ros::Publisher pubSum;

    std_msgs::Header velodyneIncomingMessageHeader;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud_lidar;
    pcl::PointCloud<PointType>::Ptr         gtTravGrid;
    pcl::PointCloud<PointType>::Ptr         gtTravGrid_lidar;
//    pcl::PointCloud<PointType>::Ptr         predTravGrid;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds_clone;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds_lidar;
    pcl::PointCloud<PointType>::Ptr         currTravGrid;
    pcl::PointCloud<PointType>::Ptr         currTravGrid_lidar;

    cv::Mat leftImage;
    cv::Mat leftImage_hsv;

    std::vector<int> enqueuedCloudsSizes;

//    This vector of vector contains the which points belong to which grid.
//    It's sort of a parallel vector to the vector of points of the Velodyne PointCloud
    std::vector<std::vector<pcl::PointXYZRGB *>> cellPointsBuckets;
    std::vector<std::vector<pcl::PointXYZRGB *>> currveloBuckets;

    std::ofstream data_storage;

    DatasetStats dataset_stats;

    int frame_cont = 0;

    // Transform Listener
    tf::TransformListener listener;
    tf::StampedTransform transform;

    Stats stats;

    int64 start_time;
    int64 velotime=-1, cametime=-1;


public:
    ProduceFeatures() {

        // update and complete paths with the base path
        base_dir_path += "vgta+/";
        data_path = base_dir_path + data_path;

        // subscribers
        subImageLeft = nh.subscribe<sensor_msgs::Image>(leftImageTopic, 5, &ProduceFeatures::leftImageHandler, this);
        subVelodyneCloud =  nh.subscribe<sensor_msgs::PointCloud2>(pointCloudTopic, 5, &ProduceFeatures::velodyneCloudHandler, this);

        // publishers
        pubTraversabilityGridCentersAsCloud = nh.advertise<sensor_msgs::PointCloud2>(trueTraversabilityTopic, 1);
        pubSum = nh.advertise<sensor_msgs::PointCloud2>(integratedCloudsTopic, 1);

        initializationValue();

        ROS_WARN("---- PARAMS: ");
        ROS_INFO(" -- show projected grid: %s", ( show_grid_projection_on_image_flag ? "ON" : "OFF") );
        ROS_INFO(" -- radius of attention: %f", grid.radius_of_attention);
        ROS_INFO(" -- curvity buckets num: %d", curvity_buckets_num_singlescan);
        ROS_INFO(" -- data file path     : %s", data_path.c_str());

    }

    ~ProduceFeatures() {
        if (data_storage.is_open())
            data_storage.close();
    }

    void initializationValue()
    {
        IOUtils::checkPaths(base_dir_path);

        velodyneRGBCloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        velodyneRGBCloud_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds_clone.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        gtTravGrid.reset(new pcl::PointCloud<PointType>());
        gtTravGrid_lidar.reset(new pcl::PointCloud<PointType>());
        currTravGrid.reset(new pcl::PointCloud<PointType>());
        currTravGrid_lidar.reset(new pcl::PointCloud<PointType>());

        enqueuedCloudsSizes.resize(cloudsQueue_fixed_size);

        // init occupancy cloud
        gtTravGrid->points.resize(grid.num_cells_per_side_squared);
        gtTravGrid_lidar->points.resize(grid.num_cells_per_side_squared);
        currTravGrid->points.resize(grid.num_cells_per_side_squared);
        currTravGrid_lidar->points.resize(grid.num_cells_per_side_squared);

        cellPointsBuckets.resize(grid.num_cells_per_side_squared);
        currveloBuckets.resize(grid.num_cells_per_side_squared);

        // set all grid values to unknown
        resetData();

        // open the file for writing the features
        data_storage = std::ofstream(data_path);
    }

    void resetData() {
        int idx;
        for (int col=0; col<grid.num_cells_per_side; ++col) {
            for (int row = 0; row < grid.num_cells_per_side; ++row) {
                idx = GridManagement::getIndexOfBelongingCellCenter(row, col, grid.num_cells_per_side);
                gtTravGrid->points[idx].z           = LOWEST_Z_VALUE;
                currTravGrid->points[idx].z         = LOWEST_Z_VALUE;
                currTravGrid_lidar->points[idx].z   = LOWEST_Z_VALUE;

                gtTravGrid->points[idx].intensity         = UNKNOWN_CELL_LABEL;
                currTravGrid->points[idx].intensity       = UNKNOWN_CELL_LABEL;
                currTravGrid_lidar->points[idx].intensity = UNKNOWN_CELL_LABEL;
            }
        }

        // clear each grid "linked list", in order to discriminate empty grids
        for (auto &bucket: cellPointsBuckets) bucket.clear();
        for (auto &bucket: currveloBuckets)   bucket.clear();

        stats.resetStats();

    }

    void velodyneCloudHandler(const sensor_msgs::PointCloud2ConstPtr & msgIn) {

        /// store the header to synchronize the output cloud with the incoming cloud, and store it
        velodyneIncomingMessageHeader = msgIn->header;
        pcl::fromROSMsg(*msgIn, *velodyneRGBCloud_lidar);

        velotime = GETTIME( velodyneIncomingMessageHeader.stamp.sec, velodyneIncomingMessageHeader.stamp.nsec );

        if ( velotime == cametime )
            script();
        else return;

    }

    void leftImageHandler(const sensor_msgs::ImageConstPtr & msgIn) {
        leftImage = cv_bridge::toCvCopy(msgIn, sensor_msgs::image_encodings::BGR8)->image;

        if(leftImage.empty()) {
            std::cout << "LEFT IMAGE IS EMPTY. ABORTING" << std::endl;
            return;
        }
        else {
            std::cout << "LEFT IMAGE is not empty!" << std::endl;
        }

        cv::Mat blurred;
        cv::GaussianBlur(leftImage, blurred, cv::Size(11,11), 0, 0);

        cv::cvtColor(blurred, leftImage_hsv, cv::COLOR_BGR2HSV);

        cametime = GETTIME( msgIn->header.stamp.sec, msgIn->header.stamp.nsec );

        if ( velotime == cametime )
            script();
        else return;

    }



    bool updateLidarToMapTransform() {
        try{listener.lookupTransform(mapFrame, lidarFrame, ros::Time(0), transform); }
        catch (tf::TransformException &ex){ ROS_ERROR("Transform Failure."); return false; }
        return true;
    }

    void updateGridBottomRight_map() {
        gridBottomRight.x = (float) transform.getOrigin().x() - grid.half_num_cells_per_side * grid.resolution - grid.half_resolution;
        gridBottomRight.y = (float) transform.getOrigin().y() - grid.half_num_cells_per_side * grid.resolution - grid.half_resolution;
    }


    void updateGridCellsCoordinates() {
        int idx;
        float x, y;
        for (int col=0; col<grid.num_cells_per_side; ++col) {
            for (int row = 0; row < grid.num_cells_per_side; ++row) {
                idx = GridManagement::getIndexOfBelongingCellCenter(row, col, grid.num_cells_per_side);
                x = (float) col * grid.resolution + gridBottomRight.x;
                y = (float) row * grid.resolution + gridBottomRight.y;
                gtTravGrid->points[idx].x = x ;
                gtTravGrid->points[idx].y = y ;
                currTravGrid->points[idx].x = x;
                currTravGrid->points[idx].y = y;
            }
        }
    }

    void getGridInLidarFrame() {
        gtTravGrid->header.stamp = 0;
        gtTravGrid->header.frame_id = mapFrame;
        pcl_ros::transformPointCloud(lidarFrame, *gtTravGrid, *gtTravGrid_lidar, listener);
    }

    void updateGridCellsElevation() {
        for (int cell_idx=0; cell_idx<grid.num_cells_per_side_squared; ++cell_idx) {
            if (ISUNKNOWN(gtTravGrid->points[cell_idx].intensity))
                // remove points which are unknown (only for visualization)
                gtTravGrid->points[cell_idx].z = HIGHEST_Z_VALUE;
            else
                // if not empty, compute the average elevation of the points inside the cell
                gtTravGrid->points[cell_idx].z = getAvgElevationOfPointsInCell(cellPointsBuckets[cell_idx]);
        }
    }

    /** now that points are sorted among the cells of the grid we can compute the features of each cell **/
    void produceFeaturesForLearning() {
        dataset_stats.resetStats( grid.num_cells_per_side_squared );
        Features feature;

        /// calculate normal of the whole scene
        computeScenePlaneNormal(integratedClouds, feature);

        for (int i = 0; i < grid.num_cells_per_side_squared; ++i) {
            feature.reset();
            if (getCellFeatures(cellPointsBuckets[i], feature, false,
                                gtTravGrid->points[i],
                                leftImage_hsv,
                                currTravGrid_lidar->points[i],
                                dataset_stats)) {
//                    std::cout << "curvity: " << feature.curvity << std::endl;
                    std::cout << "color_avg: " << feature.color_avg  << (feature.color_avg>=0 ? " -------------- " : "") << std::endl;
                    std::cout << "color_sdev: " << feature.color_sdev << std::endl;
                writeToFile(&feature);
            }
            else
                dataset_stats.emptyCellAmount_increment();
        }

        if (show_grid_projection_on_image_flag) {
            cameraLeft.showGrid(leftImage, currTravGrid_lidar, currveloBuckets);
        }

        dataset_stats.setTotPoints();
        dataset_stats.printDatasetStats();
    }

    void writeToFile(Features *feature) {
        // COVARIANCE-BASED
        data_storage << feature->linearity << " ";            // 0
        data_storage << feature->planarity << " ";            // 1
        data_storage << feature->sphericity << " ";           // 2
        data_storage << feature->omnivariance << " ";         // 3
        data_storage << feature->anisotropy << " ";           // 4
        data_storage << feature->eigenentropy << " ";         // 5
        data_storage << feature->sum_of_eigenvalues << " ";   // 6
        data_storage << feature->curvature << " ";            // 7

        // ROUGHNESS-BASED
        data_storage << feature->angle << " ";                // 8
        data_storage << feature->goodness_of_fit << " ";      // 9
        data_storage << feature->roughness << " ";            // 10
        data_storage << feature->average_elevation << " ";    // 11

        // NORMAL VECTOR-BASED
        data_storage << feature->normal_vector.x << " " << feature->normal_vector.y << " " << feature->normal_vector.z << " ";    // 12, 13, 14
        data_storage << feature->unevenness << " ";           // 15
        data_storage << feature->surface_density << " ";      // 16

        // Z_DIFF feature
        data_storage << feature->z_diff << " ";               // 17

        // DENSITY FEATURE
        data_storage << feature->internal_density << " ";               // 18


        // CURVITY FEATURE
        data_storage << feature->curvity << " ";               // 19

        // COLOR FEATURE
        data_storage << feature->color_avg << " ";               // 20
        data_storage << feature->color_sdev << " ";               // 21

//        data_storage << feature->color_hsv[0] << " ";               // 21
//        data_storage << feature->color_hsv[1] << " ";               // 21
//        data_storage << feature->color_hsv[2] << " ";               // 21
        data_storage << feature->volume << " ";               //

        data_storage << feature->label << std::endl;          // 22
    }




    void script() {
        /// for time measurements
        start_time = cv::getTickCount();

        /// transform lidar cloud to mapFrame in order to integrate it with the previous clouds
        velodyneRGBCloud_lidar->header.frame_id=lidarFrame;
        pcl_ros::transformPointCloud(mapFrame,                          // target frame
                                     ros::Time(0),
                                     *velodyneRGBCloud_lidar,   // cloud in
                                     lidarFrame,                        // fixed frame
                                     *velodyneRGBCloud,              // cloud out
                                     listener);

        /// integrate the clouds, expressed in the static map frame
        integrateClouds(velodyneRGBCloud, enqueuedCloudsSizes, integratedClouds);

        if (frame_cont<cloudsQueue_fixed_size || leftImage.empty()) {
            ROS_WARN_STREAM("cannot predict yet or left image is empty -> Ignoring data (frame_cont: "
                            + std::to_string(frame_cont) + " / " + std::to_string(cloudsQueue_fixed_size) + ")");
            frame_cont++;
            return;
        }

        /// transform the integrated clouds to the lidar frame (for points sorting)
        transformIntegratedCloudsToLidarFrame(integratedClouds, integratedClouds_lidar, velodyneRGBCloud->header.stamp, listener);

        ///
        resetData();

        if (!updateLidarToMapTransform()) return;

        updateGridBottomRight_map();

        updateGridCellsCoordinates();

        GridManagement::getGroundTruth(integratedClouds, integratedClouds_lidar, gtTravGrid, gridBottomRight, grid);


        // sort the cloud integrating points caught in subsequent scans among the cells of cellPointsBucket
        // in the grid, points are expressed in the map frame (global)
        // this grid will be used to compute features
//        *integratedClouds_clone = *integratedClouds;
        integratedClouds_clone->resize(integratedClouds->points.size());
        for ( size_t i=0; i < integratedClouds->points.size(); i++) integratedClouds_clone->points[i] = integratedClouds->points[i];
        GridManagement::sortPointsInGrid(integratedClouds_clone, integratedClouds_lidar, gtTravGrid, cellPointsBuckets,gridBottomRight, grid, true);
//        GridManagement::sortPointsInGrid(integratedClouds, integratedClouds_lidar, cellPointsBuckets, gridBottomRight, grid);

        // sort the cloud integrating points caught in subsequent scans among the cells of cellPointsBucket
        // in the grid, points are expressed in the velodyne frame (dynamic)
        GridManagement::sortPointsInGrid(velodyneRGBCloud, velodyneRGBCloud_lidar, currTravGrid, currveloBuckets, gridBottomRight, grid, false);

        // only for visualization
        for (int i=0; i<grid.num_cells_per_side_squared; ++i) {
            if (currveloBuckets[i].size() < (size_t)min_points_in_bucket_to_project_to_camera) {
                currTravGrid->points[i].intensity = UNKNOWN_CELL_LABEL;
                currTravGrid->points[i].z = HIGHEST_Z_VALUE;
            }
        }
        updateGridCellsElevation();

        ParamServer::getGridInLidarFrame(currTravGrid, currTravGrid_lidar, &listener);

        /// produce feature for each valid cell
        produceFeaturesForLearning();

        getGridInLidarFrame();

        publishClouds();

        frame_cont++;

        int64 simulation_duration = static_cast<int64>(1000.0f * (cv::getTickCount() - start_time) / cv::getTickFrequency());
        float maxbagrate = 100.0f / (float) simulation_duration;
        ROS_WARN_STREAM("simulation_duration: " + std::to_string(simulation_duration) + "    -> max bag rate is: " + std::to_string(maxbagrate) );

    }

    void publishClouds()
    {
        if (show_grid_projection_on_image_flag)
            publishCloud(&pubTraversabilityGridCentersAsCloud, currTravGrid_lidar, velodyneIncomingMessageHeader.stamp, lidarFrame );
        else
            publishCloud(&pubTraversabilityGridCentersAsCloud, gtTravGrid, velodyneIncomingMessageHeader.stamp, mapFrame );

        publishCloud(&pubSum, integratedClouds, velodyneIncomingMessageHeader.stamp, mapFrame );
    }

};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "trav_analysis");

    ProduceFeatures PF;

    ROS_INFO("\033[1;32m----> Produce Features Started.\033[0m");

    ros::spin();

    return 0;
}