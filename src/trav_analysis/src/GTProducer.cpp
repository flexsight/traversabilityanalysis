#include "utility.h"

class GTProducer : public ParamServer
{

    ros::Subscriber subVelodyneCloud;

    ros::Publisher pubTraversabilityGridCentersAsCloud;
//    ros::Publisher pubSum;

    std_msgs::Header velodyneIncomingMessageHeader;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud_lidar;
    pcl::PointCloud<PointType>::Ptr         gtTravGrid;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds_lidar;

    std::vector<int> enqueuedCloudsSizes;


//    This vector of vector contains the which points belong to which grid.
//    It's sort of a parallel vector to the vector of points of the Velodyne PointCloud
    std::vector<std::vector<pcl::PointXYZRGB *>> cellPointsBuckets;
    std::vector<std::vector<pcl::PointXYZRGB *>> currveloBuckets;

    // Transform Listener
    tf::TransformListener listener;
    tf::StampedTransform transform;

    int frame_cont = 0;





public:
    GTProducer() {

        // update and complete paths with the base path
        subVelodyneCloud = nh.subscribe<sensor_msgs::PointCloud2>(pointCloudTopic, 1, &GTProducer::velodyneCloudHandler, this);

        // publishers
        pubTraversabilityGridCentersAsCloud = nh.advertise<sensor_msgs::PointCloud2>(trueTraversabilityTopic, 1);
//        pubSum = nh.advertise<sensor_msgs::PointCloud2>(integratedCloudsTopic, 1);

        initializationValue();

        ROS_WARN("---- PARAMS: ");
        ROS_INFO(" -- radius of attention: %f", grid.radius_of_attention);
    }

    virtual ~GTProducer() {
    }

    void initializationValue() {

        velodyneRGBCloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        velodyneRGBCloud_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        gtTravGrid.reset(new pcl::PointCloud<PointType>());

        // init occupancy cloud
        gtTravGrid->points.resize(grid.num_cells_per_side_squared);

        cellPointsBuckets.resize(grid.num_cells_per_side_squared);
        currveloBuckets.resize(grid.num_cells_per_side_squared);
        cellIsPredictable.resize(grid.num_cells_per_side_squared);

        enqueuedCloudsSizes.resize(cloudsQueue_fixed_size, 0);
    }

    void resetData() {
        for (int idx=0; idx<grid.num_cells_per_side_squared; ++idx) {
            gtTravGrid->points[idx].z           = LOWEST_Z_VALUE;
            gtTravGrid->points[idx].intensity   = UNKNOWN_CELL_LABEL;
        }
        for (auto &bucket: cellPointsBuckets) bucket.clear();
        for (auto &bucket: currveloBuckets)   bucket.clear();
    }






    void velodyneCloudHandler(const sensor_msgs::PointCloud2ConstPtr & msgIn) {
        /// store the header to synchronize the output cloud with the incoming cloud, and store it
        velodyneIncomingMessageHeader = msgIn->header;
        pcl::fromROSMsg(*msgIn, *velodyneRGBCloud_lidar);

        script();
    }


    void transformIntegratedCloudsToLidarFrame() {
        integratedClouds->header.frame_id = mapFrame;
        integratedClouds->header.stamp = velodyneRGBCloud->header.stamp;
        pcl_ros::transformPointCloud(lidarFrame,                // target frame
                                     ros::Time(0),
                                     *integratedClouds,     // cloud in
                                     mapFrame,                      // source frame / fixed frame
                                     *integratedClouds_lidar,    // cloud out
                                     listener);
    }

    void updateGridCellsElevation() {
        for (int cell_idx=0; cell_idx<grid.num_cells_per_side_squared; ++cell_idx) {
            if (ISUNKNOWN(gtTravGrid->points[cell_idx].intensity))
                // remove points which are unknown (only for visualization)
                gtTravGrid->points[cell_idx].z = HIGHEST_Z_VALUE;
//            else
//                // if not empty, compute the average elevation of the points inside the cell
//                gtTravGrid->points[cell_idx].z = getAvgElevationOfPointsInCell(cellPointsBuckets[cell_idx]);
        }
    }

    void updateGridBottomRight_map() {
        gridBottomRight.x = (float) transform.getOrigin().x() - grid.half_num_cells_per_side * grid.resolution - grid.half_resolution;
        gridBottomRight.y = (float) transform.getOrigin().y() - grid.half_num_cells_per_side * grid.resolution - grid.half_resolution;
    }


    void updateGridCellsCoordinates() {
        float x, y;
        for (int col=0; col<grid.num_cells_per_side; ++col) {
            for (int row = 0; row < grid.num_cells_per_side; ++row) {
                int idx = GridManagement::getIndexOfBelongingCellCenter(row, col, grid.num_cells_per_side);
                x = (float) col * grid.resolution + gridBottomRight.x;
                y = (float) row * grid.resolution + gridBottomRight.y;
                gtTravGrid->points[idx].x = x ;
                gtTravGrid->points[idx].y = y ;
            }
        }
    }



    void script() {

        /// transform lidar cloud to mapFrame in order to integrate it with the previous clouds
        velodyneRGBCloud_lidar->header.frame_id=lidarFrame;
        pcl_ros::transformPointCloud(mapFrame,                          // target frame
                                     ros::Time(0),
                                     *velodyneRGBCloud_lidar,   // cloud in
                                     lidarFrame,                        // fixed frame
                                     *velodyneRGBCloud,              // cloud out
                                     listener);

        /// integrate the clouds, expressed in the static map frame
        integrateClouds(velodyneRGBCloud, enqueuedCloudsSizes, integratedClouds);

        if (frame_cont<cloudsQueue_fixed_size)     {
            ROS_WARN_STREAM("integrating -> Ignoring data (frame_cont: "
                            + std::to_string(frame_cont) + " / " + std::to_string(cloudsQueue_fixed_size) + ")");
            frame_cont++;
            return;
        }

        /// transform the integrated clouds to the lidar frame (for points sorting)
        transformIntegratedCloudsToLidarFrame();

        ///
        resetData();

        if (!updateLidarToMapTransform(listener, transform)) return;

        updateGridBottomRight_map();

        updateGridCellsCoordinates();

        /// for each grid cell compute the belonging pointcloud's points and the true label (ground truth)
        GridManagement::getGroundTruth(integratedClouds, integratedClouds_lidar, gtTravGrid, gridBottomRight, grid);

        updateGridCellsElevation();

        publishClouds();

        frame_cont++;
    }




    void publishClouds()
    {
//        publishCloud(&pubSum, integratedClouds, velodyneIncomingMessageHeader.stamp, mapFrame );
        publishCloud(&pubTraversabilityGridCentersAsCloud, gtTravGrid, velodyneIncomingMessageHeader.stamp, mapFrame );
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "trav_analysis");

    GTProducer GTP;

    ROS_INFO("\033[1;32m----> Ground Truth Producer Started.\033[0m");

    ros::spin();

    return 0;
}