#include "utility.h"

class Analyzer : public ParamServer
{

    ros::Subscriber subPredictedTraversabilityAsCloud;
    ros::Subscriber subTrueTraversabilityAsCloud;
    ros::Publisher pubResults;

    std_msgs::Header incomingMessageHeader;

    pcl::PointCloud<PointType>::Ptr         gtTravGrid;
    pcl::PointCloud<PointType>::Ptr         predTravGrid;
    pcl::PointCloud<PointType>::Ptr         resultsGrid;


    int64 truetime=-1, predtime=-1;

    Stats stats;

    std::ofstream out_stats_file;

public:
    Analyzer() {

        // subscribers
        subTrueTraversabilityAsCloud = nh.subscribe<sensor_msgs::PointCloud2>(trueTraversabilityTopic, 1, &Analyzer::trueGridHandler, this);
        subPredictedTraversabilityAsCloud = nh.subscribe<sensor_msgs::PointCloud2>(predictedTraversabilityTopic, 1, &Analyzer::predGridHandler, this);

        // publishers
        pubResults = nh.advertise<sensor_msgs::PointCloud2>("trav_analysis/results", 1);

        initializationValue();

//        ROS_WARN("---- PARAMS: ");
//        ROS_INFO(" -- radius of attention: %f", global_map_side);

        out_stats_file = std::ofstream("stats_scen00.txt");

    }

    void initializationValue() {

        gtTravGrid.reset(new pcl::PointCloud<PointType>());
        predTravGrid.reset(new pcl::PointCloud<PointType>());
        resultsGrid.reset(new pcl::PointCloud<PointType>());

        // init occupancy cloud
        gtTravGrid->points.resize(grid.num_cells_per_side_squared);
        predTravGrid->points.resize(grid.num_cells_per_side_squared);
        resultsGrid->points.resize(grid.num_cells_per_side_squared);
    }

    void trueGridHandler(const sensor_msgs::PointCloud2ConstPtr & msgIn) {
        incomingMessageHeader = msgIn->header;
        /// store the header to synchronize the output cloud with the incoming cloud, and store it
        pcl::fromROSMsg(*msgIn, *gtTravGrid);
        truetime = msgIn->header.stamp.sec * 1000000000 + msgIn->header.stamp.nsec;
        if ( truetime == predtime )
            script();
        else return;

    }

    void predGridHandler(const sensor_msgs::PointCloud2ConstPtr & msgIn) {
        /// store the header to synchronize the output cloud with the incoming cloud, and store it
        pcl::fromROSMsg(*msgIn, *predTravGrid);
        predtime = msgIn->header.stamp.sec * 1000000000 + msgIn->header.stamp.nsec;
        if ( truetime == predtime )
            script();
        else return;
    }

    void script() {
        ROS_WARN_STREAM("script started!");

        stats.resetStats();
        

        PointType *result_cell;
        PointType *cell;
       
        for (int i=0; i<grid.num_cells_per_side_squared; i++) {
            cell = &(gtTravGrid->points[i]);
            result_cell = &(resultsGrid->points[i]);
            
            if (cell->intensity == predTravGrid->points[i].intensity)
                result_cell->intensity = 0;
            else
                result_cell->intensity = 1;
            
            result_cell->x = cell->x;
            result_cell->y = cell->y;
            result_cell->z = cell->z;
            stats.updatePredLabelStats(gtTravGrid->points[i].intensity, predTravGrid->points[i].intensity);
        }

        stats.printStats();


        out_stats_file << stats.accuracy << std::endl;

        publishCloud(&pubResults, resultsGrid, incomingMessageHeader.stamp, mapFrame);

    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "trav_analysis");

    Analyzer Co;

    ROS_INFO("\033[1;32m----> Analyzer Started.\033[0m");

    ros::spin();

    return 0;
}