#include "utility.h"

class Composer : public ParamServer
{

    ros::Subscriber subPredictedTraversabilityAsCloud;
    ros::Publisher pubComposer;

    std_msgs::Header velodyneIncomingMessageHeader;

    pcl::PointCloud<PointType>::Ptr  velodyneRGBCloud_lidar;
    pcl::PointCloud<PointType>::Ptr         predTravGrid;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  composedGrid;
    pcl::PointCloud<PointType>::Ptr    predTravGrid_mapFrame;

    std::vector<int> state;

    // Transform Listener
//    tf::TransformListener listener;
//    tf::StampedTransform transform;


public:
    Composer() {

        // subscribers
        subPredictedTraversabilityAsCloud = nh.subscribe<sensor_msgs::PointCloud2>(predictedTraversabilityTopic, 1, &Composer::predGridHandler, this);

        // publishers
        pubComposer = nh.advertise<sensor_msgs::PointCloud2>("trav_analysis/composedGrid", 1);

        initializationValue();

        ROS_WARN("---- PARAMS: ");
        ROS_INFO(" -- radius of attention: %f", global_map_side);

    }

    void initializationValue() {
        // velodyneRGBCloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        velodyneRGBCloud_lidar.reset(new pcl::PointCloud<PointType>());

        predTravGrid.reset(new pcl::PointCloud<PointType>());
        predTravGrid_mapFrame.reset(new pcl::PointCloud<PointType>());

        composedGrid.reset(new pcl::PointCloud<pcl::PointXYZRGB>());

        // init occupancy cloud
        predTravGrid->points.resize(grid.num_cells_per_side_squared);
        predTravGrid_mapFrame->points.resize(grid.num_cells_per_side_squared);

        cellIsPredictable.resize(grid.num_cells_per_side_squared);

        int num_cells_per_side = (int) (global_map_side/grid.resolution);
        float offset = (float) num_cells_per_side / 2.0f * grid.resolution + grid.half_resolution;

        composedGrid->points.resize(num_cells_per_side*num_cells_per_side);

        state.resize(num_cells_per_side*num_cells_per_side);
        for (int i=0; i<num_cells_per_side*num_cells_per_side; ++i) state[i]=0;

        float x, y;
        for (int col=0; col < num_cells_per_side; ++col) {
            for (int row = 0; row < num_cells_per_side; ++row) {
                int idx = row * num_cells_per_side + col;
                x = (float) col * grid.resolution - offset;
                y = (float) row * grid.resolution - offset;
                composedGrid->points[idx].x = x ;
                composedGrid->points[idx].y = y ;
                composedGrid->points[idx].z = max_point_height;
                composedGrid->points[idx].r = 0;
                composedGrid->points[idx].g = 0;
                composedGrid->points[idx].b = 255;
            }
        }
    }

    void predGridHandler(const sensor_msgs::PointCloud2ConstPtr & msgIn) {
        ROS_WARN_STREAM("cloud to compose with received!");
        /// store the header to synchronize the output cloud with the incoming cloud, and store it
        velodyneIncomingMessageHeader = msgIn->header;
        pcl::fromROSMsg(*msgIn, *predTravGrid);

        int num_cells_per_side = (int) std::floor(global_map_side/grid.resolution);
        float offset = (float) num_cells_per_side / 2.0f * grid.resolution + grid.half_resolution;

        for (auto &p: predTravGrid->points) {

            float crow = (p.y + offset) / grid.resolution;
            float ccol = (p.x + offset) / grid.resolution;

            int row = std::floor(crow);
            int col = std::floor(ccol);

            if (row<0 || row>=num_cells_per_side || col<0 || col>=num_cells_per_side) { continue; }

            int idx = row * num_cells_per_side + col;

                 if (p.intensity == TRAV_CELL_LABEL)     state[idx] += 1;
            else if (p.intensity == NOT_TRAV_CELL_LABEL) state[idx] -= 1;

            if (state[idx] > min_points_in_bucket_to_project_to_camera) state[idx] = min_points_in_bucket_to_project_to_camera;
            else if (state[idx] < -min_points_in_bucket_to_project_to_camera) state[idx] = -min_points_in_bucket_to_project_to_camera;

        }

        for (int i=0; i<num_cells_per_side*num_cells_per_side; ++i) {
            if (state[i] >= 3) {
                composedGrid->points[i].r = 255;
                composedGrid->points[i].g = 255;
                composedGrid->points[i].b = 255;
            }
            else if (state[i] <= -3) {
                composedGrid->points[i].r = 255;
                composedGrid->points[i].g = 0;
                composedGrid->points[i].b = 0;
            }
            else {
                composedGrid->points[i].r = 0;
                composedGrid->points[i].g = 0;
                composedGrid->points[i].b = 255;
            }
        }

        publishClouds();
    }



    void publishClouds()
    {
        publishCloud(&pubComposer, composedGrid, velodyneIncomingMessageHeader.stamp, mapFrame);
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "trav_analysis");

    Composer Co;

    ROS_INFO("\033[1;32m----> Composer Started.\033[0m");

    ros::spin();

    return 0;
}