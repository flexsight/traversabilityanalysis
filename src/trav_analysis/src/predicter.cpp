#include "utility.h"

class Predicter : public ParamServer
{

    ros::Subscriber subVelodyneCloud;
    ros::Subscriber subImageLeft;

    ros::Publisher pubPredictedTraversabilityAsCloud;
    ros::Publisher pubSum;

    std_msgs::Header velodyneIncomingMessageHeader;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud_lidar;
    pcl::PointCloud<PointType>::Ptr         predTravGrid;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds_clone;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds_lidar;
    pcl::PointCloud<PointType>::Ptr         currTravGrid;
    pcl::PointCloud<PointType>::Ptr         currTravGrid_lidar;

    std::vector<int> enqueuedCloudsSizes;

    cv::Mat leftImage;
    cv::Mat leftImage_hsv;

//    This vector of vector contains the which points belong to which grid.
//    It's sort of a parallel vector to the vector of points of the Velodyne PointCloud
    std::vector<std::vector<pcl::PointXYZRGB *>> cellPointsBuckets;
    std::vector<std::vector<pcl::PointXYZRGB *>> currveloBuckets;

    cv::Ptr<cv::ml::SVM> svm;
    int features_num;

    std::vector<float> min, p2p;         // needed to normalize new data

    // Transform Listener
    tf::TransformListener listener;
    tf::StampedTransform transform;

    int frame_cont = 0;

    int64 start_time, simulation_duration, svm_latency;
    int64 velotime=-1, cametime=-1;
    float max_bag_rate;

    std::ofstream out_stats_file;






public:
    Predicter() {

        // update and complete paths with the base path
        base_dir_path += "vgta+/";
        normalization_config_path = base_dir_path + normalization_config_path;
        svm_model_path = base_dir_path + svm_model_path;

        // subscribers
        subImageLeft = nh.subscribe<sensor_msgs::Image>(leftImageTopic, 5, &Predicter::leftImageHandler, this);
        subVelodyneCloud = nh.subscribe<sensor_msgs::PointCloud2>(pointCloudTopic, 1, &Predicter::velodyneCloudHandler, this);

        // publishers
        pubPredictedTraversabilityAsCloud = nh.advertise<sensor_msgs::PointCloud2>(predictedTraversabilityTopic, 1);
        pubSum = nh.advertise<sensor_msgs::PointCloud2>(integratedCloudsTopic, 1);

        initializationValue();
        IOUtils::loadNormalizationConfig(normalization_config_path, features_num, min, p2p);

        ROS_WARN("---- PARAMS: ");
        ROS_INFO(" -- stats output path  : %s", out_stats_path.c_str());
        ROS_INFO(" -- show projected grid: %s", ( show_grid_projection_on_image_flag ? "ON" : "OFF") );
        ROS_INFO(" -- radius of attention: %f", grid.radius_of_attention);
        ROS_INFO(" -- svm model path     : %s", svm_model_path.c_str());
        ROS_INFO(" -- data config path   : %s", normalization_config_path.c_str());
        ROS_INFO(" -- features_num       : %d", features_num);
        ROS_INFO(" -- curvity buckets num: %d", curvity_buckets_num_singlescan);
        ROS_INFO(" -- max point height   : %f", max_point_height);

        ROS_INFO("\033[1;32m----> svm model correctly loaded\033[0m");
    }

    virtual ~Predicter() {
        if (out_stats_file.is_open())
            out_stats_file.close();
    }

    void initializationValue() {
        IOUtils::checkPaths(base_dir_path);


        /// LOAD SVM
        svm = cv::ml::SVM::load(svm_model_path);
        assert(svm->isTrained());

        features_num = svm->getVarCount();

        // stack the indexes of the features that svm will use
        for (int i=0; i<23; ++i) {
            if(columnIsRequested(i))
                feats_indexes.push_back(i);
        }


        out_stats_file = std::ofstream(out_stats_path);

        velodyneRGBCloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        velodyneRGBCloud_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds_clone.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        predTravGrid.reset(new pcl::PointCloud<PointType>());
        currTravGrid.reset(new pcl::PointCloud<PointType>());
        currTravGrid_lidar.reset(new pcl::PointCloud<PointType>());


        // init occupancy cloud
        predTravGrid->points.resize(grid.num_cells_per_side_squared);
        currTravGrid->points.resize(grid.num_cells_per_side_squared);
        currTravGrid_lidar->points.resize(grid.num_cells_per_side_squared);

        cellPointsBuckets.resize(grid.num_cells_per_side_squared);
        currveloBuckets.resize(grid.num_cells_per_side_squared);
        cellIsPredictable.resize(grid.num_cells_per_side_squared);

        enqueuedCloudsSizes.resize(cloudsQueue_fixed_size, 0);
    }



    void resetData() {
        for (int idx=0; idx<grid.num_cells_per_side_squared; ++idx) {
            predTravGrid->points[idx].z       = LOWEST_Z_VALUE;
            currTravGrid->points[idx].z       = LOWEST_Z_VALUE;
            currTravGrid_lidar->points[idx].z = LOWEST_Z_VALUE;

            predTravGrid->points[idx].intensity       = UNKNOWN_CELL_LABEL;
            currTravGrid->points[idx].intensity       = UNKNOWN_CELL_LABEL;
            currTravGrid_lidar->points[idx].intensity = UNKNOWN_CELL_LABEL;

            cellIsPredictable[idx] = 0;

            // clear each grid "linked list", in order to discriminate empty grids
            cellPointsBuckets[idx].clear();
            currveloBuckets[idx].clear();
        }

        features_matrix = cv::Mat::zeros(grid.num_cells_per_side_squared, features_num, CV_32F);
        predictions_vector = cv::Mat::zeros(grid.num_cells_per_side_squared, 1, CV_32FC1);

    }


    void velodyneCloudHandler(const sensor_msgs::PointCloud2ConstPtr & msgIn) {
        /// store the header to synchronize the output cloud with the incoming cloud, and store it
        velodyneIncomingMessageHeader = msgIn->header;
        pcl::fromROSMsg(*msgIn, *velodyneRGBCloud_lidar);

        velotime = GETTIME( velodyneIncomingMessageHeader.stamp.sec, velodyneIncomingMessageHeader.stamp.nsec );

        if ( velotime == cametime )
            script();
        else return;

        script();
    }

    void leftImageHandler(const sensor_msgs::ImageConstPtr & msgIn) {
        leftImage = cv_bridge::toCvCopy(msgIn, sensor_msgs::image_encodings::BGR8)->image;

        if(leftImage.empty()) {
            std::cout << "LEFT IMAGE IS EMPTY. ABORTING" << std::endl;
            return;
        }
        else {
            std::cout << "LEFT IMAGE is not empty!" << std::endl;
        }

        cv::Mat blurred;
        cv::GaussianBlur(leftImage, blurred, cv::Size(11,11), 0, 0);

        cv::cvtColor(blurred, leftImage_hsv, cv::COLOR_BGR2HSV);

        cametime = GETTIME( msgIn->header.stamp.sec, msgIn->header.stamp.nsec );

        if ( velotime == cametime )
            script();
        else return;

    }

    void updateGridCellsElevation() {
        for (int cell_idx=0; cell_idx<grid.num_cells_per_side_squared; ++cell_idx) {
            /// if the cell has a suff num of points, set the avg elev to the pred cell
            if (cellPointsBuckets[cell_idx].size() >= (size_t) min_points_in_bucket)
                predTravGrid->points[cell_idx].z = getAvgElevationOfPointsInCell(cellPointsBuckets[cell_idx]);
            /// else remove points which are unknown (only for visualization)
            else {
                predTravGrid->points[cell_idx].intensity = UNKNOWN_CELL_LABEL;
                predTravGrid->points[cell_idx].z         = HIGHEST_Z_VALUE;
            }
        }
    }

    void printSimulationInfo() const {
        std::stringstream max_bag_rate_ss;
        max_bag_rate_ss << std::fixed << std::setprecision(2) << max_bag_rate;

//        stats.printStats("FRAME #" + std::to_string(frame_cont) + ":");
        ROS_WARN_STREAM("FRAME #" + std::to_string(frame_cont) + ":");
        ROS_WARN_STREAM("   -- svm latency: " + std::to_string(svm_latency) + " ms");
        ROS_WARN_STREAM("   -- simulation_duration: " + std::to_string(simulation_duration));
        ROS_WARN_STREAM("   ----> max bag rate is: " + max_bag_rate_ss.str() );
        ROS_INFO_STREAM("");
    }


    void updateGridCellsCoordinates() {
        float x, y;
        for (int col=0; col<grid.num_cells_per_side; ++col) {
            for (int row = 0; row < grid.num_cells_per_side; ++row) {
                int idx = GridManagement::getIndexOfBelongingCellCenter(row, col, grid.num_cells_per_side);
                x = (float) col * grid.resolution + gridBottomRight.x;
                y = (float) row * grid.resolution + gridBottomRight.y;
                predTravGrid->points[idx].x = x;
                predTravGrid->points[idx].y = y;
                currTravGrid->points[idx].x = x;
                currTravGrid->points[idx].y = y;
            }
        }
    }

    void fillFeatureMatrix( ) {
        DatasetStats dataset_stats;
        Features feature;
        cv::Mat feature_vector(1, features_num, CV_32F);

        /// calculate normal of the whole scene
        computeScenePlaneNormal(integratedClouds, feature);

        /// build the feature matrix x (in the indexes vector store the correspondant index of the cell)
        for (int i = 0; i < grid.num_cells_per_side_squared; ++i) {
            feature.reset();

            /// if this function returns false, then the class should be unknown (already set as default)
            if ( getCellFeaturesPred(cellPointsBuckets[i], feature, false,
                                     predTravGrid->points[i],
//                                     leftImage_hsv,
                                     currTravGrid_lidar->points[i],
                                     dataset_stats)
                    ) {

                /// build the feature vector corresponding to the current field cell
                buildFeature(feature_vector, feature);
                /// copy the row
                for (int j=0; j<features_num; j++) features_matrix.at<float>(i, j) = feature_vector.at<float>(0, j);


                /// COLOR CELL BASED ON FEATURE VALUE
                /// you need to comment out predictFeatureMatrix(), setPredictedValuesToPredictedCloud() & filterOutliers(
//                predTravGrid->points[i].intensity = feature.curvity / curvity_buckets_num_singlescan * 205 + 50;

                cellIsPredictable[i] = 1;
            }
        }

        if (show_grid_projection_on_image_flag) {
            cameraLeft.showGrid(leftImage, currTravGrid_lidar, currveloBuckets);
        }
    }

    void predictFeatureMatrix() {
        int64 start = cv::getTickCount();
        svm->predict(features_matrix, predictions_vector);
        svm_latency = static_cast<int64>(1000.0f * (float)(cv::getTickCount() - start) / cv::getTickFrequency());
    }

    void setPredictedValuesToPredictedCloud() {
        for (int i=0; i<grid.num_cells_per_side_squared; i++) {
            if (cellIsPredictable[i])
                predTravGrid->points[i].intensity =
                        (predictions_vector.at<float>(i, 0) > 0 ? (float) TRAV_CELL_LABEL : (float) NOT_TRAV_CELL_LABEL);
            else
                predTravGrid->points[i].intensity = UNKNOWN_CELL_LABEL;
        }
    }

    void filterOutliers() {
        int cell_idx, newrow, newcol, road_count, non_road_count;
        float pred_label;

        for (int row = 0; row < grid.num_cells_per_side; ++row) {
            for (int col = 0; col < grid.num_cells_per_side; ++col) {
                road_count = 0; non_road_count = 0;

                for (int o1=-1; o1<2; o1++) {
                    for (int o2=-1; o2<2; o2++) {
                        if (o1 || o2) { // skip the cell with o1==0 && o2==0 (it's the current cell)
                            newrow = row + o1; if ( newrow < 0 || newrow >= grid.num_cells_per_side ) continue;
                            newcol = col + o2; if ( newcol < 0 || newcol >= grid.num_cells_per_side ) continue;
                            cell_idx = GridManagement::getIndexOfBelongingCellCenter(newrow, newcol, grid.num_cells_per_side);
                            pred_label = predTravGrid->points[cell_idx].intensity;
                            if (ISTRAVERSABLE(pred_label)) road_count++;
                            else if (ISNOTTRAVERSABLE(pred_label)) non_road_count++;
                        }
                    }
                }
                cell_idx = GridManagement::getIndexOfBelongingCellCenter(row, col, grid.num_cells_per_side);
                PointType *pred_cell = &(predTravGrid->points[cell_idx]);

                if ( !ISUNKNOWN(pred_cell->intensity) ) {
                    if (ISTRAVERSABLE(pred_cell->intensity)) road_count += predicted_label_weight;
                    else non_road_count += predicted_label_weight;

                    pred_cell->intensity = (road_count > non_road_count ? (float) TRAV_CELL_LABEL : (float) NOT_TRAV_CELL_LABEL);
                }
//                else if ( road_count + non_road_count > min_neighbors_to_propagate_labels )
//                    pred_cell->intensity = (road_count > non_road_count ? (float) TRAV_CELL_LABEL : (float) NOT_TRAV_CELL_LABEL);
//                 else pred_cell->intensity = UNKNOWN_CELL_LABEL;

            }
        }
    }

    void predictEachField() {
        int64 s;
        s = cv::getTickCount();
        fillFeatureMatrix();
        if (timing_verbosity>=2)  printDur("fillFeatureMatrix", s);

        s = cv::getTickCount();
        predictFeatureMatrix();
        if (timing_verbosity>=2)  printDur("predictFeatureMatrix", s);

        s = cv::getTickCount();
        setPredictedValuesToPredictedCloud();
        if (timing_verbosity>=2)  printDur("setPredictedValuesToPredictedCloud", s);

        s = cv::getTickCount();
        /// integrate information coming from the neighbors!
        filterOutliers();
        if (timing_verbosity>=2)  printDur("filterOutliers", s);
    }

    void buildFeature(cv::Mat &x, Features &feature) {
        float *start = &(feature.linearity);

        for (int i=0; i < (int) feats_indexes.size()    ; ++i)
            x.at<float>(0, i) =  (*(start + feats_indexes[i]) - min[i]) / p2p[i];
    }


    static void printDur(const std::string& msg, int64 s) {
        int64 d;
        d = static_cast<int64>(1000.0f * (float)(cv::getTickCount() - s) / cv::getTickFrequency());
        ROS_INFO_STREAM("duration of " + msg + " : " + std::to_string(d) + " ms");
    }



    void script() {

        int64 s;

        /// for time measurements
        start_time = cv::getTickCount();

        s = cv::getTickCount();
        /// transform lidar cloud to mapFrame in order to integrate it with the previous clouds
        velodyneRGBCloud_lidar->header.frame_id = lidarFrame;
        std::cout << "input cloud transformed to map frame at time " << ros::Time::now() << std::endl;
        pcl_ros::transformPointCloud(mapFrame,                          // target frame
                                     ros::Time(0),
                                     *velodyneRGBCloud_lidar,   // cloud in
                                     lidarFrame,                        // fixed frame
                                     *velodyneRGBCloud,              // cloud out
                                     listener);
        if (timing_verbosity>=2)  printDur("transformPointCloud1", s);

        /// integrate the clouds, expressed in the static map frame
        s = cv::getTickCount();
        integrateClouds(velodyneRGBCloud, enqueuedCloudsSizes, integratedClouds);
        if (timing_verbosity >= 2) printDur("integrateClouds", s);

        if ( frame_cont < cloudsQueue_fixed_size )     {
            ROS_WARN_STREAM("cannot predict yet or left image is empty -> Ignoring data (frame_cont: "
                            + std::to_string(frame_cont) + " / " + std::to_string(cloudsQueue_fixed_size) + ")");
            frame_cont++;
            return;
        }

        /// transform the integrated clouds to the lidar frame (for points sorting)
        s = cv::getTickCount();
        transformIntegratedCloudsToLidarFrame(integratedClouds, integratedClouds_lidar, velodyneRGBCloud->header.stamp, listener);
        if (timing_verbosity>=2) printDur("transformIntegratedCloudsToLidarFrame", s);

        s = cv::getTickCount();
        resetData();
        if (timing_verbosity>=2)  printDur("resetData", s);


        s = cv::getTickCount();
        if (!updateLidarToMapTransform(listener, transform)) return;
        if (timing_verbosity>=2)  printDur("updateLidarToMapTransform", s);


        s = cv::getTickCount();
        updateGridBottomRight_map(transform);
        updateGridCellsCoordinates();
        if (timing_verbosity>=2)  printDur("updateGrid things", s);

        /// for each field (grid cell) compute the belonging pointcloud's points and the true label - for evaluation
        s = cv::getTickCount();

//        MODIFY STARTS HERE and declaration on the beginning of integratedClouds_clone
//        *integratedClouds_clone = *integratedClouds;
        integratedClouds_clone->resize(integratedClouds->points.size());
        for ( size_t i=0; i < integratedClouds->points.size(); i++) integratedClouds_clone->points[i] = integratedClouds->points[i];
//        GridManagement::sortPointsInGrid(integratedClouds, integratedClouds_lidar, cellPointsBuckets, gridBottomRight, grid);
        GridManagement::sortPointsInGrid(integratedClouds_clone, integratedClouds_lidar, predTravGrid, cellPointsBuckets, gridBottomRight, grid, true);
//        MODIFY END


        GridManagement::sortPointsInGrid(velodyneRGBCloud, velodyneRGBCloud_lidar, currTravGrid, currveloBuckets, gridBottomRight, grid, false);
        if (timing_verbosity>=2)  printDur("sortPointsInGrid", s);

        for (int i=0; i<grid.num_cells_per_side_squared; ++i) {
            if (currveloBuckets[i].size() < (size_t) min_points_in_bucket_to_project_to_camera) {
                currTravGrid->points[i].intensity = UNKNOWN_CELL_LABEL;
                currTravGrid->points[i].z = HIGHEST_Z_VALUE;
            }
        }

        s = cv::getTickCount();
        updateGridCellsElevation();
        if (timing_verbosity>=2)  printDur("updateGridCellsElevation", s);

        s = cv::getTickCount();
        getGridInLidarFrame(currTravGrid, currTravGrid_lidar, &listener);
        if (timing_verbosity>=2)  printDur("getGridInLidarFrame", s);

        /// predict each field traversability label
        predictEachField();

        /// for time measurements
        simulation_duration = static_cast<int64>(1000.0f * (float)(cv::getTickCount() - start_time) / cv::getTickFrequency());
        max_bag_rate = 100.0f / (float) simulation_duration;

        printSimulationInfo();

        publishClouds();

        frame_cont++;

    }


    void publishClouds()
    {
        if (show_grid_projection_on_image_flag)
            publishCloud(&pubPredictedTraversabilityAsCloud, currTravGrid_lidar, velodyneIncomingMessageHeader.stamp, lidarFrame );
        else
            publishCloud(&pubPredictedTraversabilityAsCloud, predTravGrid, velodyneIncomingMessageHeader.stamp, mapFrame );

        publishCloud(&pubSum, integratedClouds, velodyneIncomingMessageHeader.stamp, mapFrame );
//        publishCloud(&pubSum, integratedClouds_clone, velodyneIncomingMessageHeader.stamp, mapFrame );
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "trav_analysis");

    Predicter OS;

    ROS_INFO("\033[1;32m----> Online System Started.\033[0m");

    ros::spin();

    return 0;
}