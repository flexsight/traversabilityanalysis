#include "utility.h"

class OnlineSystem : public ParamServer
{

    ros::Subscriber subVelodyneCloud;
    ros::Subscriber subImageLeft;

    ros::Publisher pubTraversabilityGridCentersAsCloud;
    ros::Publisher pubPredictedTraversabilityAsCloud;
    ros::Publisher pubSum;

    std_msgs::Header velodyneIncomingMessageHeader;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  velodyneRGBCloud_lidar;
    pcl::PointCloud<PointType>::Ptr         gtTravGrid;
    pcl::PointCloud<PointType>::Ptr         gtTravGrid_lidar;
    pcl::PointCloud<PointType>::Ptr         predTravGrid;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr  integratedClouds_lidar;
    pcl::PointCloud<PointType>::Ptr         currTravGrid;
    pcl::PointCloud<PointType>::Ptr         currTravGrid_lidar;

    cv::Mat leftImage;
    cv::Mat leftImage_hsv;

    std::vector<int> enqueuedCloudsSizes;


//    This vector of vector contains the which points belong to which grid.
//    It's sort of a parallel vector to the vector of points of the Velodyne PointCloud
    std::vector<std::vector<pcl::PointXYZRGB *>> cellPointsBuckets;
    std::vector<std::vector<pcl::PointXYZRGB *>> currveloBuckets;

    cv::Ptr<cv::ml::SVM> svm;
    int features_num;

    std::ifstream input_normalization_config_file;
    std::vector<float> min, p2p;         // needed to normalize new data

    // Transform Listener
    tf::TransformListener listener;
    tf::StampedTransform transform;

    Stats stats;

    int frame_cont = 0;

    int64 start_time, simulation_duration, svm_latency;
    float max_bag_rate;

    int64 velotime=-1, cametime=-1;

    std::ofstream out_stats_file;





public:
    OnlineSystem() {

        // update and complete paths with the base path
        base_dir_path += "vgta+/";
        normalization_config_path = base_dir_path + normalization_config_path;
        svm_model_path = base_dir_path + svm_model_path;

        // subscribers
        subImageLeft = nh.subscribe<sensor_msgs::Image>(leftImageTopic, 1, &OnlineSystem::leftImageHandler, this);

        subVelodyneCloud = nh.subscribe<sensor_msgs::PointCloud2>(pointCloudTopic, 1, &OnlineSystem::velodyneCloudHandler, this);

        // publishers
        pubTraversabilityGridCentersAsCloud = nh.advertise<sensor_msgs::PointCloud2>(trueTraversabilityTopic, 1);
        pubPredictedTraversabilityAsCloud = nh.advertise<sensor_msgs::PointCloud2>(predictedTraversabilityTopic, 1);
        pubSum = nh.advertise<sensor_msgs::PointCloud2>(integratedCloudsTopic, 1);

        initializationValue();
        loadNormalizationConfig();

        ROS_WARN("---- PARAMS: ");
        ROS_INFO(" -- stats output path  : %s", out_stats_path.c_str());
        ROS_INFO(" -- show projected grid: %s", ( show_grid_projection_on_image_flag ? "ON" : "OFF") );
        ROS_INFO(" -- radius of attention: %f", grid.radius_of_attention);
        ROS_INFO(" -- svm model path     : %s", svm_model_path.c_str());
        ROS_INFO(" -- data config path   : %s", normalization_config_path.c_str());
        ROS_INFO(" -- features_num       : %d", features_num);
        ROS_INFO(" -- curvity buckets num: %d", curvity_buckets_num_singlescan);
        ROS_INFO(" -- max point height   : %f", max_point_height);

        ROS_INFO("\033[1;32m----> svm model correctly loaded\033[0m");
    }

    virtual ~OnlineSystem() {
        if (input_normalization_config_file.is_open())
            input_normalization_config_file.close();
        if (out_stats_file.is_open())
            out_stats_file.close();
    }

    void initializationValue() {
        IOUtils::checkPaths(base_dir_path);

        svm = cv::ml::SVM::load(svm_model_path);
        assert(svm->isTrained());

        features_num = svm->getVarCount();

        /// stack the indexes of the features that svm will use
        for (int i=0; i<22; ++i) {
            if(columnIsRequested(i))
                feats_indexes.push_back(i);
        }


        out_stats_file = std::ofstream(out_stats_path);

        velodyneRGBCloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        velodyneRGBCloud_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        integratedClouds_lidar.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
        gtTravGrid.reset(new pcl::PointCloud<PointType>());
        gtTravGrid_lidar.reset(new pcl::PointCloud<PointType>());
        predTravGrid.reset(new pcl::PointCloud<PointType>());
        currTravGrid.reset(new pcl::PointCloud<PointType>());
        currTravGrid_lidar.reset(new pcl::PointCloud<PointType>());



        // init occupancy cloud
        gtTravGrid->points.resize(grid.num_cells_per_side_squared);
        gtTravGrid_lidar->points.resize(grid.num_cells_per_side_squared);
        predTravGrid->points.resize(grid.num_cells_per_side_squared);
        currTravGrid->points.resize(grid.num_cells_per_side_squared);
        currTravGrid_lidar->points.resize(grid.num_cells_per_side_squared);

        cellPointsBuckets.resize(grid.num_cells_per_side_squared);
        currveloBuckets.resize(grid.num_cells_per_side_squared);
        cellIsPredictable.resize(grid.num_cells_per_side_squared);

        enqueuedCloudsSizes.resize(cloudsQueue_fixed_size, 0);

        min.resize(features_num);
        p2p.resize(features_num);
    }



    void loadNormalizationConfig() {
        input_normalization_config_file = std::ifstream(normalization_config_path);
        std::string line;
        int start, end, col_cont;


        // load min
        start = 0, col_cont = 0;
        getline (input_normalization_config_file, line);
        assert(line.length()>0);
        for (int i=0; i<line.length(); i++) {
            assert(col_cont<features_num);
            if (line.at(i) == ' ' || line.at(i) == '\n' || i==line.length()-1) {
                end = i;
                min[col_cont] = (float) std::stod(line.substr(start, end));
                start = i;
                col_cont++;
            }
        }

        // load p2p
        start = 0, col_cont = 0;
        getline (input_normalization_config_file, line);
        assert(line.length()>0);
        for (int i=0; i<line.length(); i++) {
            assert(col_cont<features_num);
            if (line.at(i) == ' ' || line.at(i) == '\n' || i==line.length()-1) {
                end = i;
                p2p[col_cont] = (float) std::stod(line.substr(start, end));
                start = i;
                col_cont++;
            }
        }

    }

    void resetData() {
        int idx;
        for (int col=0; col<grid.num_cells_per_side; ++col) {
            for (int row = 0; row < grid.num_cells_per_side; ++row) {
                idx = getIndexOfBelongingCellCenter(row, col);
                gtTravGrid->points[idx].z   = LOWEST_Z_VALUE;
                predTravGrid->points[idx].z = LOWEST_Z_VALUE;
                currTravGrid->points[idx].z = LOWEST_Z_VALUE;
                currTravGrid_lidar->points[idx].z = LOWEST_Z_VALUE;
                gtTravGrid->points[idx].intensity   = UNKNOWN_CELL_LABEL;
                predTravGrid->points[idx].intensity = UNKNOWN_CELL_LABEL;
                currTravGrid->points[idx].intensity = UNKNOWN_CELL_LABEL;
                currTravGrid_lidar->points[idx].intensity = UNKNOWN_CELL_LABEL;
            }
        }

        // clear each grid "linked list", in order to discriminate empty grids
        for (auto &bucket: cellPointsBuckets) bucket.clear();
        for (auto &bucket: currveloBuckets)   bucket.clear();

        features_matrix = cv::Mat::zeros(grid.num_cells_per_side_squared, features_num, CV_32F);
        predictions_vector = cv::Mat::zeros(grid.num_cells_per_side_squared, 1, CV_32FC1);
        for (int i=0; i<grid.num_cells_per_side_squared; i++) cellIsPredictable[i] = 0;

        stats.resetStats();

    }





    void leftImageHandler(const sensor_msgs::ImageConstPtr & msgIn) {
        leftImage = cv_bridge::toCvCopy(msgIn, sensor_msgs::image_encodings::BGR8)->image;
        cv::Mat blurred;
        cv::GaussianBlur(leftImage, blurred, cv::Size(5,5), 0, 0);

        cv::cvtColor(blurred, leftImage_hsv, cv::COLOR_BGR2HSV);

        cametime = msgIn->header.stamp.sec * 1000000000 + msgIn->header.stamp.nsec;

        if ( velotime == cametime )
            script();
        else return;

    }

    void velodyneCloudHandler(const sensor_msgs::PointCloud2ConstPtr & msgIn) {
        /// store the header to synchronize the output cloud with the incoming cloud, and store it
        velodyneIncomingMessageHeader = msgIn->header;
        pcl::fromROSMsg(*msgIn, *velodyneRGBCloud_lidar);

        std::cout << "pointcloud received" << std::endl;

        velotime = velodyneIncomingMessageHeader.stamp.sec * 1000000000 + velodyneIncomingMessageHeader.stamp.nsec;

        if ( velotime == cametime )
            script();
        else if (true) {
            ROS_WARN_STREAM("script started without cameras");
            script();
        }

    }



    void transformIntegratedCloudsToLidarFrame() {
        integratedClouds->header.frame_id = mapFrame;
        integratedClouds->header.stamp = velodyneRGBCloud->header.stamp;
        pcl_ros::transformPointCloud(lidarFrame,                // target frame
                                     ros::Time(0),
                                     *integratedClouds,     // cloud in
                                     mapFrame,                      // source frame / fixed frame
                                     *integratedClouds_lidar,    // cloud out
                                     listener);
    }

    void updateGridCellsElevation() {
        float cellAvgElevation;

        for (int cell_idx=0; cell_idx<grid.num_cells_per_side_squared; ++cell_idx) {
            // if not empty, compute the average elevation of the points inside the cell
            if (!cellPointsBuckets[cell_idx].empty()) {
                cellAvgElevation = getAvgElevationOfPointsInCell(cellPointsBuckets[cell_idx]);
                gtTravGrid->points[cell_idx].z = cellAvgElevation;

                // if the cell has a suff num of points, set the avg elev to the pred cell
                // otherwise remove the pred cell from the visual scene
                if (cellPointsBuckets[cell_idx].size() > min_points_in_bucket)
                    predTravGrid->points[cell_idx].z = cellAvgElevation;
                else {
                    predTravGrid->points[cell_idx].intensity = UNKNOWN_CELL_LABEL;
                    predTravGrid->points[cell_idx].z         = HIGHEST_Z_VALUE;    // removed for visual cleaning
                }
            }
                // otherwise remove both ground truth and predicted cells from the scene
            else {
                gtTravGrid->points[cell_idx].intensity   = UNKNOWN_CELL_LABEL;
                predTravGrid->points[cell_idx].intensity = UNKNOWN_CELL_LABEL;
                predTravGrid->points[cell_idx].z         = HIGHEST_Z_VALUE; // removed for visual cleaning
            }
        }
    }

    void printSimulationInfo() {
        std::stringstream max_bag_rate_ss;
        max_bag_rate_ss << std::fixed << std::setprecision(2) << max_bag_rate;

        stats.printStats("FRAME #" + std::to_string(frame_cont) + ":");
        ROS_WARN_STREAM("   -- svm latency: " + std::to_string(svm_latency) + " ms");
        ROS_WARN_STREAM("   -- simulation_duration: " + std::to_string(simulation_duration));
        ROS_WARN_STREAM("   ----> max bag rate is: " + max_bag_rate_ss.str() );
        ROS_INFO_STREAM("");
    }

    void updateGridBottomRight_map() {
        gridBottomRight.x = (float) transform.getOrigin().x() - grid.half_num_cells_per_side * grid.resolution - grid.half_resolution;
        gridBottomRight.y = (float) transform.getOrigin().y() - grid.half_num_cells_per_side * grid.resolution - grid.half_resolution;
    }


    void updateGridCellsCoordinates() {
        float x, y;
        for (int col=0; col<grid.num_cells_per_side; ++col) {
            for (int row = 0; row < grid.num_cells_per_side; ++row) {
                int idx = getIndexOfBelongingCellCenter(row, col);
                x = (float) col * grid.resolution + gridBottomRight.x;
                y = (float) row * grid.resolution + gridBottomRight.y;
                gtTravGrid->points[idx].x = x ;
                gtTravGrid->points[idx].y = y ;
                predTravGrid->points[idx].x = x;
                predTravGrid->points[idx].y = y;
                currTravGrid->points[idx].x = x;
                currTravGrid->points[idx].y = y;
            }
        }
    }

    void filterOutliers() {
        int cell_idx, newrow, newcol, road_count, non_road_count;
        float true_label, pred_label;

        for (int row = 0; row < grid.num_cells_per_side; ++row) {
            for (int col = 0; col < grid.num_cells_per_side; ++col) {
                road_count = 0; non_road_count = 0;

                for (int o1=-1; o1<2; o1++) {
                    for (int o2=-1; o2<2; o2++) {
                        if (o1 || o2) { // skip the cell with o1==0 && o2==0 (it's the current cell)
                            newrow = row + o1; if ( newrow < 0 || newrow >= grid.num_cells_per_side ) continue;
                            newcol = col + o2; if ( newcol < 0 || newcol >= grid.num_cells_per_side ) continue;
                            cell_idx = getIndexOfBelongingCellCenter(newrow, newcol);
                            pred_label = predTravGrid->points[cell_idx].intensity;
                            if (ISTRAVERSABLE(pred_label)) road_count++;
                            else if (ISNOTTRAVERSABLE(pred_label)) non_road_count++;
                        }
                    }
                }
                cell_idx = getIndexOfBelongingCellCenter(row, col);
                PointType *pred_cell = &(predTravGrid->points[cell_idx]);

                if ( !ISUNKNOWN(pred_cell->intensity) ) {
                    if (ISTRAVERSABLE(pred_cell->intensity)) road_count += predicted_label_weight;
                    else non_road_count += predicted_label_weight;

                    pred_cell->intensity = (road_count > non_road_count ? TRAV_CELL_LABEL : NOT_TRAV_CELL_LABEL);
                }
                else if ( road_count + non_road_count > min_neighbors_to_propagate_labels )
                    pred_cell->intensity = (road_count > non_road_count ? TRAV_CELL_LABEL : NOT_TRAV_CELL_LABEL);
                else pred_cell->intensity = UNKNOWN_CELL_LABEL;

                /// update stats with the current field cell outcome
                true_label = gtTravGrid->points[cell_idx].intensity;
                stats.updatePredLabelStats(true_label, pred_cell->intensity);

            }
        }
    }

    void setPredictedValuesToPredictedCloud() {
        for (int i=0; i<grid.num_cells_per_side_squared; i++) {
            if (cellIsPredictable[i])
                predTravGrid->points[i].intensity =
                        (predictions_vector.at<float>(i, 0) > 0 ? TRAV_CELL_LABEL : NOT_TRAV_CELL_LABEL);
            else
                predTravGrid->points[i].intensity = UNKNOWN_CELL_LABEL;
        }
    }

    void fillFeatureMatrix( ) {
        DatasetStats dataset_stats;
        Features feature;
        cv::Mat feature_vector(1, features_num, CV_32F);


        /// calculate normal of the whole scene
        computeScenePlaneNormal(integratedClouds, feature);

        /// build the feature matrix x (in the indexes vector store the correspondant index of the cell)
        for (int i = 0; i < grid.num_cells_per_side_squared; ++i) {
            feature.reset();

            /// if this function returns false, then the class should be unknown (already set as default)
            if ( getCellFeatures(cellPointsBuckets[i], feature, false,
                                 gtTravGrid->points[i],
                                 leftImage_hsv,
                                 currTravGrid_lidar->points[i],
                                 dataset_stats)
                    ) {

                /// build the feature vector corresponding to the current field cell
                buildFeature(feature_vector, feature);
                /// copy the row
                for (int j=0; j<features_num; j++) features_matrix.at<float>(i, j) = feature_vector.at<float>(0, j);

                cellIsPredictable[i] = 1;
            }
        }



    }

    void predictFeatureMatrix() {
        int64 start = cv::getTickCount();
        svm->predict(features_matrix, predictions_vector);
        svm_latency = static_cast<int64>(1000.0f * (float)(cv::getTickCount() - start) / cv::getTickFrequency());
    }

    void predictEachField() {

        fillFeatureMatrix();

        predictFeatureMatrix();

        setPredictedValuesToPredictedCloud();

        /// integrate information coming from the neighbors!
        filterOutliers();

        /// remove points which are unknown (only for visualization)
//        removeUnknownCellsFromGrid(gtTravGrid);
//        removeUnknownCellsFromGrid(predTravGrid);

        if (show_grid_projection_on_image_flag) {
            cameraLeft.showGrid(leftImage, currTravGrid_lidar, currveloBuckets);
        }


    }

    void buildFeature(cv::Mat &x, Features &feature) {
        float *start = &(feature.linearity);

        for (int i=0; i < (int) feats_indexes.size()    ; ++i)
            x.at<float>(0, i) =  (*(start + feats_indexes[i]) - min[i]) / p2p[i];
    }






    void script() {

        /// for time measurements
        start_time = cv::getTickCount();

        /// transform lidar cloud to mapFrame in order to integrate it with the previous clouds
        //pcl_ros::transformPointCloud(mapFrame,  *velodyneRGBCloud_lidar, *velodyneRGBCloud, listener);
        velodyneRGBCloud_lidar->header.frame_id=lidarFrame;
        pcl_ros::transformPointCloud(mapFrame,                          // target frame
                                     ros::Time(0),
                                     *velodyneRGBCloud_lidar,   // cloud in
                                     lidarFrame,                        // fixed frame
                                     *velodyneRGBCloud,              // cloud out
                                     listener);

        /// integrate the clouds, expressed in the static map frame
        integrateClouds(velodyneRGBCloud, enqueuedCloudsSizes, integratedClouds);

        if (frame_cont<cloudsQueue_fixed_size /*|| leftImage.empty()*/)     {
            ROS_WARN_STREAM("cannot predict yet or left image is empty -> Ignoring data (frame_cont: "
                            + std::to_string(frame_cont) + " / " + std::to_string(cloudsQueue_fixed_size) + ")");
            frame_cont++;
            return;
        }

        /// transform the integrated clouds to the lidar frame (for points sorting)
        transformIntegratedCloudsToLidarFrame();

        ///
        resetData();


        if (!updateLidarToMapTransform(listener, transform)) return;

        updateGridBottomRight_map();

        updateGridCellsCoordinates();

        /// for each field (grid cell) compute the belonging pointcloud's points and the true label - for evaluation
        sortPointsInGrid(integratedClouds, integratedClouds_lidar, predTravGrid, cellPointsBuckets, false);
        sortPointsInGrid(velodyneRGBCloud, velodyneRGBCloud_lidar, currTravGrid, currveloBuckets, false);
        for (int i=0; i<grid.num_cells_per_side_squared; ++i) {
            if (currveloBuckets[i].size()<min_points_in_bucket_to_project_to_camera) {
                currTravGrid->points[i].intensity = UNKNOWN_CELL_LABEL;
                currTravGrid->points[i].z = HIGHEST_Z_VALUE;
            }
        }

        updateGridCellsElevation();

        getGridInLidarFrame(gtTravGrid, gtTravGrid_lidar, &listener);
        getGridInLidarFrame(currTravGrid, currTravGrid_lidar, &listener);

        /// predict each field traversability label
        predictEachField();

        publishClouds();

        /// for time measurements
        simulation_duration = static_cast<int64>(1000.0f * (float)(cv::getTickCount() - start_time) / cv::getTickFrequency());
        max_bag_rate = 100.0f / (float) simulation_duration;

        printSimulationInfo();

        frame_cont++;
    }




    void publishClouds()
    {
        if (show_grid_projection_on_image_flag)
            publishCloud(&pubTraversabilityGridCentersAsCloud, currTravGrid_lidar, velodyneIncomingMessageHeader.stamp, lidarFrame );
        else
            publishCloud(&pubTraversabilityGridCentersAsCloud, gtTravGrid, velodyneIncomingMessageHeader.stamp, mapFrame );
        publishCloud(&pubPredictedTraversabilityAsCloud, predTravGrid, velodyneIncomingMessageHeader.stamp, mapFrame );
        publishCloud(&pubSum, integratedClouds, velodyneIncomingMessageHeader.stamp, mapFrame );
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "trav_analysis");

    OnlineSystem OS;

    ROS_INFO("\033[1;32m----> Online System Started.\033[0m");

    ros::spin();

    return 0;
}