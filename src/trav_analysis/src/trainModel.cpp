#include "utility.h"

#define MAT_TYPE CV_32SC1
#define PRED_MAT_TYPE CV_32F

#define READ_TYPE int32_t
#define PRED_READ_TYPE float

class TrainModel : public ParamServer
{
    std::ifstream input_data_file;
    std::ofstream output_config_file;
    std::ofstream best_params_file;
    cv::Ptr<cv::ml::SVM> /*linear_svm = nullptr, poly_svm = nullptr,*/ rbf_svm = nullptr;

    std::vector<std::vector<float>> dataset;
    size_t dataset_rows_num{}, features_num{}, label_idx{};
    int m_training{}, m_testing{};           // how many of the data is used as training or testing
    std::vector<float> min, p2p;         // needed to normalize training and new data

    cv::Mat X_train, y_train;
    cv::Mat X_test, y_test;
    cv::Mat X_val, y_val;
    cv::Mat pred_val;

    std::ofstream temp_file;

public:
    TrainModel() {


        data_path = base_dir_path + data_path;
        normalization_config_path = base_dir_path + normalization_config_path;
        linear_svm_path = base_dir_path + linear_svm_path;
        poly_svm_path = base_dir_path + poly_svm_path;
        rbf_svm_path = base_dir_path + rbf_svm_path;

        ROS_WARN("---- PARAMS: ");
        ROS_INFO(" -- kfolds             : %d", kfolds);
        ROS_INFO(" -- data file path     : %s", data_path.c_str());
        ROS_INFO(" -- data config path   : %s", normalization_config_path.c_str());
        initializationValue();
        ROS_INFO(" -- dataset #rows      : %lu", dataset_rows_num);
        ROS_INFO(" -- dataset #cols      : %lu", (features_num+1));
        std::stringstream cols_skipped("");
        for (int i : skip_cols_with_index) cols_skipped << i << ", ";

        ROS_INFO(" -- cols that will be skipped: %s", cols_skipped.str().c_str());

        ROS_INFO("models going to be trained: %s %s %s", (use_linear_svm ? "linear" : ""), (use_poly_svm ? "poly" : ""), (use_rbf_svm ? "rbf" : ""));

        ROS_INFO("models going to be saved  : %s %s %s", (use_linear_svm && save_linear_svm ? "linear"  : ""),
                 (use_poly_svm   && save_poly_svm   ? "poly"    : ""),
                 (use_rbf_svm    && save_rbf_svm    ? "rbf"      : ""));


        temp_file = std::ofstream("vgta+/tempfile.txt");
        trainSVM();
    }

    void initializationValue() {
        IOUtils::checkPaths(base_dir_path);

        input_data_file = std::ifstream(data_path);
        output_config_file = std::ofstream(normalization_config_path);
        best_params_file = std::ofstream(base_dir_path + "best_params_file.txt");
        getFileRowsAndCols();

        dataset.resize(dataset_rows_num);
        for (size_t i=0; i<dataset_rows_num; i++)
            dataset[i].resize(features_num+1);

        min.resize(features_num);
        p2p.resize(features_num);

        m_training = (int) (training_ratio * (float) dataset_rows_num);
        m_testing  = (int) dataset_rows_num - m_training;
    }

    void getFileRowsAndCols() {
        assert(input_data_file.is_open());
        /// compute the number of rows of the file
        dataset_rows_num = std::count(std::istreambuf_iterator<char>(input_data_file),
                                      std::istreambuf_iterator<char>(), '\n');

        assert(dataset_rows_num>0);

        /// reset the iterator over the file
        input_data_file.seekg(0, std::ios::beg);

        /// compute the number of cols of the file
        std::string line;
        assert (getline (input_data_file, line));
        features_num = 0;
        for (char c : line)
            if (c == ' ') features_num++;

        assert(features_num>1); // one because at least one feature and one label

        features_num -= (skip_cols_with_index.size());
        label_idx = features_num;

    }

    virtual ~TrainModel() {
        if (input_data_file.is_open())
            input_data_file.close();
        if (output_config_file.is_open())
            output_config_file.close();
        if (best_params_file.is_open())
            best_params_file.close();
        temp_file.close();
    }

    void printSVMTrainingStats(cv::Ptr<cv::ml::SVM> &svm, Stats &stats_train, Stats &stats_test) {

        X_train = cv::Mat(m_training, (int) features_num, CV_32F);
        y_train = cv::Mat(m_training, 1, MAT_TYPE);
        X_test = cv::Mat(m_testing, (int) features_num, CV_32F);
        y_test = cv::Mat(m_testing, 1, MAT_TYPE);

        for (int row=0; row<m_training; row++) {
            for (size_t col=0; col<features_num; col++)
                X_train.at<float>(row, col) = dataset[row][col];
            y_train.at<READ_TYPE>(row) = (READ_TYPE) dataset[row][label_idx];
        }

        for (size_t row = (size_t) m_training, i=0; row < dataset_rows_num; row++, i++) {
            for (size_t col=0; col<features_num; col++)
                X_test.at<float>(i, col) = dataset[row][col];
            y_test.at<READ_TYPE>(i) = (READ_TYPE) dataset[row][label_idx];
        }

        /// predict for train and test
        cv::Mat pred_train(X_train.rows, 1, PRED_MAT_TYPE);
        cv::Mat pred_test(X_test.rows, 1, PRED_MAT_TYPE);
        svm->predict(X_train, pred_train);
        svm->predict(X_test, pred_test);

        /// count train stats
        for (int i=0; i<y_train.rows; i++) {
            READ_TYPE label = pred_train.at<READ_TYPE>(i, 0) > 0 ? (READ_TYPE) TRAV_CELL_LABEL : (READ_TYPE) NOT_TRAV_CELL_LABEL;
            stats_train.updatePredLabelStats((float) y_train.at<READ_TYPE>(i, 0), (float) label);
        }

        /// count test stats
        for (int i=0; i<y_test.rows; i++) {
            READ_TYPE label = pred_test.at<READ_TYPE>(i, 0) > 0 ? (READ_TYPE) TRAV_CELL_LABEL : (READ_TYPE) NOT_TRAV_CELL_LABEL;
            stats_test.updatePredLabelStats((float) y_test.at<READ_TYPE>(i, 0), (float) label);
        }

        /// print train and test stats
        stats_train.printStats("-- TRAIN:");
        stats_test.printStats("-- TEST:");

    }

    void fillDataset() {
        /// reset the iterator over the file
        input_data_file.seekg(0, std::ios::beg);

        /// fill dataset reading the values from the file
        std::string line;
        int row_cont = 0;
        while (getline (input_data_file, line)) {
            int start = 0, end, col_cont = 0;
            int feat_cont=0;
            for (size_t i=0; i<line.length(); i++) {
                if (line.at(i) == ' ' || line.at(i) == '\n' || i == line.length()-1) {
                    end = i;
                    if (columnIsRequested(col_cont))
                        dataset[row_cont][feat_cont++] = stof(line.substr(start, end));
                    start = i;
                    col_cont++;
                }
            }
            row_cont ++;
        }
        input_data_file.close();
    }

    void printDatasetInfo() {

        int trav_cont = 0;
        int non_trav_cont = 0;
        int unknown_cont = 0;
        int lbl_idx = (int) dataset[0].size() - 1;

        for (auto & dataset_row : dataset) {
            if (dataset_row[lbl_idx] == TRAV_CELL_LABEL) trav_cont++;
            else if (dataset_row[lbl_idx] == NOT_TRAV_CELL_LABEL) non_trav_cont++;
            else unknown_cont++;
        }

        float rate_trav = TO_PERC(RATE(trav_cont, (int) dataset.size()));
        float rate_nontrav = TO_PERC(RATE(non_trav_cont, (int) dataset.size()));
        float rate_unk = TO_PERC(RATE(unknown_cont, (int) dataset.size()));


        ROS_WARN_STREAM("Dataset info:");
        std::cout << "traversable samples    : " << trav_cont     << "  ( " << std::setprecision(3) << rate_trav << "% )" << std::endl;
        std::cout << "not traversable samples: " << non_trav_cont << "  ( " << std::setprecision(3) << rate_nontrav << "% )" << std::endl;
        std::cout << "unknown samples        : " << unknown_cont  << "  ( " << std::setprecision(3) << rate_unk << "% )" << std::endl;
        ROS_INFO_STREAM("");


        int small = std::min(trav_cont, non_trav_cont);
        int newsize = 2*small;
        std::vector<std::vector<float>> balanced_dataset;
        balanced_dataset.resize(newsize);
        for (int i=0; i<newsize; i++)
            balanced_dataset[i].resize(features_num+1);

        trav_cont = 0;
        non_trav_cont = 0;
        int balanced_idx = 0;

        for (size_t r = 0; r < dataset_rows_num; r++) {
            if (balanced_idx>=newsize) break;

            if (dataset[r][lbl_idx] == TRAV_CELL_LABEL) {
                if (trav_cont<small) {
                    for (size_t i=0; i<features_num+1; i++) {
                        balanced_dataset[balanced_idx][i] = dataset[r][i];
                    }
                    trav_cont++;
                    balanced_idx++;
                }
            }
            else if (dataset[r][lbl_idx] == NOT_TRAV_CELL_LABEL) {
                if (non_trav_cont<small) {
                    for (size_t i=0; i<features_num+1; i++) {
                        balanced_dataset[balanced_idx][i] = dataset[r][i];
                    }
                    balanced_idx++;
                    non_trav_cont ++ ;
                }
            }
        }

        //*dataset = *balanced_dataset;
//        dataset.resize(newsize);
//        std::copy(balanced_dataset.begin(), balanced_dataset.end(), dataset.begin());
        dataset = balanced_dataset;
        dataset_rows_num = newsize;
        m_training = (int) (training_ratio * (float) dataset_rows_num);
        m_testing  = (int) dataset_rows_num - m_training;
        dataset.resize(newsize);

        trav_cont = 0;
        non_trav_cont = 0;
        unknown_cont = 0;

        for (auto & dataset_row : dataset) {
            if (dataset_row[lbl_idx] == TRAV_CELL_LABEL) trav_cont++;
            else if (dataset_row[lbl_idx] == NOT_TRAV_CELL_LABEL) non_trav_cont++;
            else unknown_cont++;
        }

        rate_trav = TO_PERC(RATE(trav_cont, (int) dataset.size()));
        rate_nontrav = TO_PERC(RATE(non_trav_cont, (int) dataset.size()));
        rate_unk = TO_PERC(RATE(unknown_cont, (int) dataset.size()));


        ROS_WARN_STREAM("Balanced dataset info:");
        std::cout << "traversable samples    : " << trav_cont     << "  ( " << std::setprecision(3) << rate_trav << "% )" << std::endl;
        std::cout << "not traversable samples: " << non_trav_cont << "  ( " << std::setprecision(3) << rate_nontrav << "% )" << std::endl;
        std::cout << "unknown samples        : " << unknown_cont  << "  ( " << std::setprecision(3) << rate_unk << "% )" << std::endl;
        ROS_INFO_STREAM("");


    }

    static void printSplittedDatasetInfo(cv::Mat data, const std::string& msg) {

        int trav_cont = 0;
        int non_trav_cont = 0;
        int unknown_cont = 0;


        for (int r = 0; r < data.rows; r++) {
            if (data.at<READ_TYPE>(r, 0) == (READ_TYPE) TRAV_CELL_LABEL) trav_cont++;
            else if (data.at<READ_TYPE>(r, 0) == (READ_TYPE) NOT_TRAV_CELL_LABEL) non_trav_cont++;
            else unknown_cont++;
        }

        ROS_WARN_STREAM(msg);
        std::cout<< "traverable samples    : " << trav_cont     << "  ( " << std::setprecision(3) << TO_PERC(RATE(trav_cont, data.rows)) << "% )" << std::endl;
        std::cout<< "not traverable samples: " << non_trav_cont << "  ( " << std::setprecision(3) << TO_PERC(RATE(non_trav_cont, data.rows)) << "% )" << std::endl;
        std::cout<< "unknown samples       : " << unknown_cont  << "  ( " << std::setprecision(3) << TO_PERC(RATE(unknown_cont, data.rows)) << "% )" << std::endl;
        ROS_INFO_STREAM("");
    }

    void computeDatasetStatistics() {
        /// find min and max for each column
        float max[features_num];

        for (size_t i=0; i<features_num; i++) {
            max[i] = FLT_MIN;
            min[i] = FLT_MAX;
            p2p[i] = .0f;
        }

        for ( int row=0; row<m_training; row++) {
            for (size_t i=0; i<features_num; i++) {
                if (dataset[row][i]>max[i]) max[i] = dataset[row][i];
                else if (dataset[row][i]<min[i]) min[i] = dataset[row][i];
            }
        }

        /// compute the peek to peek distance for each column
        for (size_t i=0; i<features_num; i++) p2p[i] = max[i] - min[i];
    }

    void normalizeDataset() {
        for (size_t row = 0; row < dataset_rows_num; row++)
            for (size_t i=0; i<features_num; i++)
                dataset[row][i] = (dataset[row][i] - min[i]) / p2p[i];
    }

    void saveNormalizationConfig() {
        for (size_t i=0; i<min.size()-1; i++)
            output_config_file << min[i] << " ";
        output_config_file << min[min.size()-1] << std::endl;

        for (size_t i=0; i<p2p.size()-1; i++)
            output_config_file << p2p[i] << " ";
        output_config_file << p2p[p2p.size()-1] << std::endl;
    }


    /// shuffle the dataset using a random distribution
    void shuffleDataset() {
        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(dataset.begin(), dataset.end(), g );
    }

    void splitDataset() {



        X_train = cv::Mat(m_training, (int) features_num, CV_32F);
        y_train = cv::Mat(m_training, 1, MAT_TYPE);
        X_test = cv::Mat(m_testing, (int) features_num, CV_32F);
        y_test = cv::Mat(m_testing, 1, MAT_TYPE);

        for (size_t row=m_training, i=0; row<dataset_rows_num; row++, i++) {
            for (size_t col=0; col<features_num; col++)
                X_test.at<float>(i, col) = dataset[row][col];
            y_test.at<READ_TYPE>(i) = (READ_TYPE) dataset[row][label_idx];
        }
    }

    float trainRbfModel(cv::Ptr<cv::ml::SVM> &model, double nu, double gamma, bool is_the_final_evaluation) {
//    float trainRbfModel(cv::Ptr<cv::ml::SVM> &modelaaa, bool is_the_final_evaluation) {
        int local_kfolds_num;
        std::vector<int> indexes;

        if (!is_the_final_evaluation) {
            assert(kfolds > 1);
            local_kfolds_num = kfolds;
            for (int i=local_kfolds_num; i>=0; i--) indexes.push_back(i);
        }
        else {
            local_kfolds_num = 1;
            indexes.push_back(1);
            indexes.push_back(1);
        }


        float sum_accuracy = .0f;
        int single_fold_samples_num = (int) m_training / local_kfolds_num;
        int val_length;
        int start_val_idx, end_val_idx;

        for (int fold_num=0; fold_num<local_kfolds_num; fold_num++) {


            model = cv::ml::SVM::create();
            model->setType(cv::ml::SVM::NU_SVC);
            model->setKernel(cv::ml::SVM::RBF);
            model->setNu(nu);
            model->setGamma(gamma);
            model->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, -1, term_criteria));

            // compute start and end validation set indexes over the whole training set
            start_val_idx = single_fold_samples_num * indexes[fold_num+1];
            end_val_idx   = std::min( indexes[fold_num] * single_fold_samples_num, m_training );
            val_length    = end_val_idx - start_val_idx;

            // resize training and validation sets
            X_train = cv::Mat(m_training - val_length, (int) features_num, CV_32F);
            y_train = cv::Mat(m_training - val_length, 1, MAT_TYPE);
            X_val = cv::Mat(val_length, (int) features_num, CV_32F);
            y_val = cv::Mat(val_length, 1, MAT_TYPE);
            pred_val = cv::Mat::zeros(cv::Size(val_length, 1), PRED_MAT_TYPE);

            /// fill X_train with the first part of the k-1 folds of the training data
            for (int row=0; row<start_val_idx; row++) {
                for (size_t col=0; col<features_num; col++)
                    X_train.at<float>(row, col) = dataset[row][col];
                y_train.at<READ_TYPE>(row) = (READ_TYPE) dataset[row][label_idx];
            }

            /// fill X_val with the only fold of the training data used for validation
            for (int row=start_val_idx, j=0; row<end_val_idx; row++, j++) {
                for (size_t col=0; col<features_num; col++)
                    X_val.at<float>(j, col) = dataset[row][col];
                y_val.at<READ_TYPE>(j) = (READ_TYPE) dataset[row][label_idx];
            }

            /// fill X_train with the last part of the k-1 folds of the training data
            for (size_t row=end_val_idx, j=start_val_idx; row < (size_t)m_training; row++, j++) {
                for (size_t col=0; col<features_num; col++)
                    X_train.at<float>(j, col) = dataset[row][col];
                y_train.at<READ_TYPE>(j) = (READ_TYPE) dataset[row][label_idx];
            }

            model->train(X_train, cv::ml::ROW_SAMPLE, y_train);

            if (!model->isTrained()) {
                ROS_ERROR_STREAM(" +++++ Model didn't train in fold " + std::to_string(fold_num) + "! +++++");
                return .0f;
            }
            try {
                model->predict(X_val, pred_val);
            } catch (cv::Exception &e) {
                std::cout << (e.msg) << std::endl;
                assert(false);
            }

            int corrects = 0;

//            temp_file << "starting fold #" << (fold_num+1) << std::endl;
//            int trav_cont=0;
//            int non_trav_cont = 0;
//            int unk_cont = 0;
//            for (int i=0; i<y_val.rows; i++) {
//                if (y_val.at<READ_TYPE>(i, 0) == (READ_TYPE) TRAV_CELL_LABEL) trav_cont++;
//                else if (y_val.at<READ_TYPE>(i, 0) == (READ_TYPE) NOT_TRAV_CELL_LABEL) non_trav_cont++;
//                else unk_cont++;
//            }

//            temp_file << "trav_cont    : " << trav_cont     << " ( " << std::setprecision(3) <<  ( ((float)trav_cont/y_val.rows)*100 ) << " )" << std::endl;
//            temp_file << "non_trav_cont: " << non_trav_cont << " ( " << std::setprecision(3) <<  ( ((float)non_trav_cont/y_val.rows)*100 ) << " )" << std::endl;
//            temp_file << "unk_cont     : " << unk_cont      << " ( " << std::setprecision(3) <<  ( ((float)unk_cont/y_val.rows)*100 ) << " )" << std::endl;

//            if (y_val.rows>0) {
            for (int i = 0; i < pred_val.rows; i++) {
//                    READ_TYPE label = pred_val.at<PRED_READ_TYPE>(i, 0) > 0 ? (READ_TYPE) TRAV_CELL_LABEL
//                                                                            : (READ_TYPE) NOT_TRAV_CELL_LABEL;
//                    temp_file << (READ_TYPE) pred_val.at<PRED_READ_TYPE>(i, 0) << " "
//                              << (READ_TYPE) y_val.at<READ_TYPE>(i, 0) << std::endl;
                if ((PRED_READ_TYPE) y_val.at<READ_TYPE>(i, 0) == pred_val.at<PRED_READ_TYPE>(i, 0)) corrects++;
            }
//            }

//            temp_file << std::endl;

            sum_accuracy += ((float) corrects / (float) pred_val.rows);
        }

        return sum_accuracy / (float) kfolds;
    }

    void customTrainRbfSVM() {

        std::stringstream nus("");    for (double nu : rbfSVM_Nu) nus << nu << ", ";
        std::stringstream gammas(""); for (double gamma : rbfSVM_Gamma) gammas << gamma << ", ";

        ROS_INFO_STREAM("");
        ROS_WARN_STREAM(" ---- Rbf kernel params: ");
        ROS_INFO_STREAM(std::setprecision(3) << "   -- Nu   : " << nus.str());
        ROS_INFO_STREAM(std::setprecision(3) << "   -- Gamma: " << gammas.str());

        std::vector<cv::Ptr<cv::ml::SVM>> rbfSVM(rbfSVM_Nu.size()*rbfSVM_Gamma.size());
        std::vector<float> accuracy(rbfSVM.size());

        size_t num_training = rbfSVM_Nu.size() * rbfSVM_Gamma.size();

        float best_accuracy = FLT_MIN;
        size_t best_rbf_model_idx = -1;

        Stats stats_train, stats_test;

        cv::Ptr<cv::ml::SVM> model;

        for (size_t i=0; i<rbfSVM_Nu.size(); i++) {
            double nu = rbfSVM_Nu[i];

            for (size_t j=0; j<rbfSVM_Gamma.size(); j++) {
                double gamma = rbfSVM_Gamma[j];
                size_t idx = i*rbfSVM_Gamma.size() + j;

//                model = cv::ml::SVM::create();
//                model->setType(cv::ml::SVM::NU_SVC);
//                model->setKernel(cv::ml::SVM::RBF);
//                model->setNu(nu);
//                model->setGamma(gamma);
//                model->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, -1, term_criteria));

                ROS_INFO("[%lu/%lu]: train started with Nu: %f, Gamma: %f", (idx+1), num_training, nu, gamma);
                auto start_time = cv::getTickCount();
                accuracy[idx] = trainRbfModel(model, nu, gamma, false);
                auto duration = static_cast<int64>(1000.0f * (float)(cv::getTickCount() - start_time) / cv::getTickFrequency());

                if (best_accuracy < accuracy[idx]) {
                    best_rbf_model_idx = idx;
                    best_accuracy = accuracy[idx];
                    ROS_INFO("[%lu/%lu]: train finished (%lu ms)  -->   \033[1;34maccuracy: %f\033[0m", (idx+1), num_training, duration, accuracy[idx]);
                }
                else
                    ROS_INFO("[%lu/%lu]: train finished (%lu ms)  -->   accuracy: %f", (idx+1), num_training, duration, accuracy[idx]);
            }
        }

        // get min!
        ROS_WARN("-- BEST MODEL PARAMETERS:");
        ROS_INFO("  -- nu   : %f", rbfSVM_Nu[floor(best_rbf_model_idx / rbfSVM_Gamma.size())]);
        ROS_INFO("  -- gamma: %f", rbfSVM_Gamma[best_rbf_model_idx % rbfSVM_Gamma.size()]);
        ROS_INFO("  -- accuracy: \033[1;32m%f\033[0m", accuracy[best_rbf_model_idx]);
        ROS_INFO("   ");

        ROS_WARN("---- NOW retraining the best model on the whole training set");

        double nu       = rbfSVM_Nu[floor(best_rbf_model_idx / rbfSVM_Gamma.size())];
        double gamma    = rbfSVM_Gamma[best_rbf_model_idx % rbfSVM_Gamma.size()];

        cv::Ptr<cv::ml::SVM> best_rbf_svm;// = cv::ml::SVM::create();
//        best_rbf_svm->setType(cv::ml::SVM::NU_SVC);
//        best_rbf_svm->setKernel(cv::ml::SVM::RBF);
//        best_rbf_svm->setNu(nu);
//        best_rbf_svm->setGamma(gamma);
//        best_rbf_svm->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, -1, term_criteria));

        ROS_INFO("---- Best RBF Train started with Nu: %f, Gamma: %f", nu, gamma);
        auto start_time = cv::getTickCount();
        trainRbfModel(best_rbf_svm, nu, gamma, true);  // no accuracy since no validation
        auto duration = static_cast<int64>(1000.0f * (float)(cv::getTickCount() - start_time) / cv::getTickFrequency());
        ROS_INFO("---- BestRBF train finished (%lu ms)", duration);

        printSVMTrainingStats(best_rbf_svm, stats_train, stats_test);
        rbf_svm = best_rbf_svm;

        best_params_file << "RBF best model parameters: " << std::endl;
        best_params_file << " ----- best nu: " << std::to_string(nu) << std::endl;
        best_params_file << " ----- best gamma: " << std::to_string(gamma) << std::endl;
        best_params_file << " ----- training accuracy: " << std::to_string(stats_train.accuracy) << std::endl;
        best_params_file << " ----- training fake_accuracy: " << std::to_string(stats_train.fake_accuracy) << std::endl;
        best_params_file << " ----- training miou: " << std::to_string(stats_train.miou) << std::endl;
        best_params_file << " ----- training f1score: " << std::to_string(stats_train.f1score) << std::endl;
        best_params_file << " ----- training FPR: " << std::to_string(stats_train.FPR) << std::endl;
        best_params_file << " ----- training TPR: " << std::to_string(stats_train.TPR) << std::endl;
        best_params_file << " ----- training FNR: " << std::to_string(stats_train.FNR) << std::endl;
        best_params_file << " ----- training TNR: " << std::to_string(stats_train.TNR) << std::endl;
        best_params_file << " ----- training UNKR: " << std::to_string(stats_train.UNKR) << std::endl;

        best_params_file << " ----- test accuracy: " << std::to_string(stats_test.accuracy) << std::endl;
        best_params_file << " ----- test fake_accuracy: " << std::to_string(stats_test.fake_accuracy) << std::endl;
        best_params_file << " ----- test miou: " << std::to_string(stats_test.miou) << std::endl;
        best_params_file << " ----- test f1score: " << std::to_string(stats_test.f1score) << std::endl;
        best_params_file << " ----- test FPR: " << std::to_string(stats_test.FPR) << std::endl;
        best_params_file << " ----- test TPR: " << std::to_string(stats_test.TPR) << std::endl;
        best_params_file << " ----- test FNR: " << std::to_string(stats_test.FNR) << std::endl;
        best_params_file << " ----- test TNR: " << std::to_string(stats_test.TNR) << std::endl;
        best_params_file << " ----- test UNKR: " << std::to_string(stats_test.UNKR) << std::endl;
        best_params_file << " ------------------------------------------ " << std::endl;
        best_params_file << std::endl;
    }

    void trainSVM() {

        fillDataset();

        printDatasetInfo();

        shuffleDataset();

        /// split the dataset in training and testing
        splitDataset();

        printSplittedDatasetInfo(y_test, "Y test info: " );

        /// compute min and p2p values only from the training set
        /// it can be arguable to determine, finally, the configs on the entire dataset ( train + test )
        computeDatasetStatistics();

        normalizeDataset();

        saveNormalizationConfig();

        ///train models
        if (use_rbf_svm) customTrainRbfSVM();

        /// save models
        if (rbf_svm == nullptr)
            std::cout << "cannot save rbf model" << std::endl;
        else
        if (use_rbf_svm && save_rbf_svm)       { rbf_svm->save(rbf_svm_path); std::cout << "rbf model saved at: " << rbf_svm_path << std::endl; }
    }

};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "trav_analysis");

    ROS_INFO("\033[1;32m----> Train Model Started.\033[0m");

    TrainModel TM;

    ros::shutdown();

    return 0;
}