import subprocess
import time
import rospy
import roslaunch
import os

def createDir(path):
    try:
        if not os.path.exists(path):
            os.mkdir(path)
    except OSError as e:
        print(e)
        exit(1)

# EXPERIMENT INFO
num_scenarios = 11
launch_file_path = '/home/fusy/repos/original_trav/src/traversability_mapping/launch/offline.launch'

# EXPERIMENT DIRECTORY INFO
dir_cwd = "/home/fusy/.ros/trav_analysis/data/"
dir_name = "exp0"
dir_path = dir_cwd + dir_name + "/"
createDir(dir_path)

# MY EXPERIMENT DIRECTORY INFO
dir_path_mine = dir_path + "original_trav/"
print(dir_path_mine)
createDir(dir_path_mine)

# BAG INFO
bag_cwd = "/home/fusy/tests/semantikitti2bag"
bag_name_base = "semantickitti_sequence"
bag_time_start = 5
bag_duration = 50
bag_rate = 0.5

params = ['rosbag', 'play', bag_name_base, "--clock", "-s " + str(bag_time_start), "-u " + str(bag_duration),
          "-r " + str(bag_rate)]

rospy.init_node('session_node', anonymous=True)
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
roslaunch.configure_logging(uuid)

for i in range(10, num_scenarios):
    bag_name = bag_name_base + str(i).zfill(2) + ".bag"
    params[2] = bag_name

    dir_final_path = dir_path_mine + "scenario" + str(i).zfill(2) + ".txt"

    cli_args = [launch_file_path, 'dirPath:='+dir_final_path]
    roslaunch_args = cli_args[1:]
    roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]

    launch = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    launch.start()
    rospy.loginfo("started")

    player_call = subprocess.Popen(params, cwd=bag_cwd)

    time.sleep(bag_duration/bag_rate)

    launch.shutdown()

    player_call.kill()

