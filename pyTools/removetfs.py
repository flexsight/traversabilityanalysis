from rosbag import Bag
import sys
import time
import rospy

topicslist = ["/velodyne_points",]
# topicslist = ["/velodyne_points", "/tf_static", "/odometry/filtered_map", "/mavros/imu/data"]

allowed_childs = ["ground_truth"]

# inbagpath = "/home/fusy/bags/office_rooftop.bag"
inbagpath = "/home/fusy/tests/semantikitti2bag/sk00.bag"
outbagpath = "/home/fusy/bags/outbag_rooftop.bag"
# outbagpath = "/home/fusy/tests/semantikitti2bag/out00.bag"

with Bag(outbagpath, "w") as outbag:
    meta = Bag(inbagpath)
    start = meta.get_start_time()
    end = meta.get_end_time()
    duration = end - start

    for topic, msg, t in meta:
        elem = (t.to_sec() - start) / duration * 100
        print("                                                           ", end="\r")
        print("{0:.3f} %".format(elem).rjust(10), end="\r")

        if topic in topicslist:
            outbag.write(topic, msg, t)
            pass
        else:
            if topic == '/tf':
                fmsg = []
                for elem in msg.transforms:
                    if elem.child_frame_id in allowed_childs:
                        elem.header.frame_id = elem.header.frame_id.replace("world", "map")
                        fmsg.append(elem)
                msg.transforms = fmsg
                outbag.write(topic, msg, t)
            # elif topic == "/odometry/filtered_map":
            #     outbag.write(topic, msg, t)
            # elif topic == "/mavros/imu/data":
            #     outbag.write(topic, msg, t)