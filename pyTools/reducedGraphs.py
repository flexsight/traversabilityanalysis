import matplotlib.pyplot as plt
import math
from matplotlib.legend import _get_legend_handles_labels

def gettime(sec, nsec):
    return sec*1000000000 + nsec

class Data:

    def fillwithzero(self, maxlen):
        while len(self.frames) < maxlen:
            self.frames.append(0)
            self.sec.append(0)
            self.nsec.append(0)
            self.accuracies.append(0)
            self.fake_accuracy.append(0)
            self.iou.append(0)
            self.f1score.append(0)
            self.FPR.append(0)
            self.TPR.append(0)
            self.FNR.append(0)
            self.TNR.append(0)
            self.simulation_duration.append(0)

    def fillWithTimeHeader(self, h_s, h_n):
        for i, (s, n) in enumerate(zip(h_s, h_n)):
            self.frames.insert(i, 0)
            self.sec.insert(i, s)
            self.nsec.insert(i, n)
            self.accuracies.insert(i, 0)
            self.fake_accuracy.insert(i, 0)
            self.iou.insert(i, 0)
            self.f1score.insert(i, 0)
            self.FPR.insert(i, 0)
            self.TPR.insert(i, 0)
            self.FNR.insert(i, 0)
            self.TNR.insert(i, 0)
            self.simulation_duration.insert(i, 0)

    def __init__(self, filepath):
        self.frames = []
        self.sec = []
        self.nsec = []
        self.accuracies = []
        self.fake_accuracy = []
        self.iou = []
        self.f1score = []
        self.FPR = []
        self.TPR = []
        self.FNR = []
        self.TNR = []
        self.simulation_duration = []
        print(filepath, end="")
        try:
            file = open(filepath)
        except:
            print(" not found!")
            return
        print(" found :)")
        for line in file:
            line = line.split(" ")
            linelen = len(line)
            self.frames.append(float(line[0]))
            self.sec.append(float(line[1]))
            self.nsec.append(float(line[2]))
            self.accuracies.append(float(line[3]))
            self.fake_accuracy.append(float(line[4]))
            self.iou.append(float(line[5]))
            self.f1score.append(float(line[6]))
            self.FPR.append(float(line[7]))
            self.TPR.append(float(line[8]))
            self.FNR.append(float(line[9]))
            self.TNR.append(float(line[10]))
            if linelen > 11:
                self.simulation_duration.append(float(line[11])/1000)
            else:
                self.simulation_duration.append(0)
        file.close()

def getreduced(vec, s):
    reduced = []
    for i in range(0, len(vec), s):
        sub = vec[i:min(i+s, len(vec))]
        avg = sum(sub) / len(sub)
        reduced.append(avg)
    return reduced


# EXPERIMENT DIRECTORY INFO
dir_cwd = "/home/fusy/.ros/trav_analysis/data/exp1/"

dir_path_shan2018_10cm = dir_cwd + "tixiao_trav_0_1/"
dir_path_shan2018_40cm = dir_cwd + "tixiao_trav_0_4/"
dir_path_stdgeom_00 = dir_cwd + "stdgeom_00_no_filter/"
dir_path_stdgeom_00_01 = dir_cwd + "stdgeom_00_01_no_filter/"
dir_path_stdgeom_allscen = dir_cwd + "stdgeom_allscen_no_filter/"
dir_path_geom_00 = dir_cwd + "geom_00/"
dir_path_geom_00_01 = dir_cwd + "geom_00_01/"
dir_path_geom_00_01_allscen = dir_cwd + "geom_allscen/"
dir_path_allfeat_00 = dir_cwd + "VGTA+_00/"
# dir_path_allfeat_00 = dir_cwd + "allfeats_00/"
dir_path_allfeat_00_01 = dir_cwd + "allfeats_00_01/"
dir_path_allfeat_allscen = dir_cwd + "allfeats_allscen/"
dir_path_3DCNN_00 = dir_cwd + "pointnet_00/"
dir_path_3DCNN_0001 = dir_cwd + "pointnet_0001/"
dir_path_3DCNN_all = dir_cwd + "pointnet_all/"

paths = [
    dir_path_shan2018_10cm,
    dir_path_shan2018_40cm,
    dir_path_stdgeom_00,
    dir_path_stdgeom_00_01,
    dir_path_stdgeom_allscen,
    dir_path_geom_00,
    dir_path_geom_00_01,
    dir_path_geom_00_01_allscen,
    dir_path_allfeat_00,
    dir_path_allfeat_00_01,
    dir_path_allfeat_allscen,
    dir_path_3DCNN_00,
    dir_path_3DCNN_0001,
    dir_path_3DCNN_all
]

legend = [
    "BGK$_{0.1m}$",
    "BGK$_{0.4m}$",
    "GTA$_{00}$",
    "GTA$_{00,01}$",
    "GTA$_{all}$",
    "GTA+$_{00}$",
    "GTA+$_{00,01}$",
    "GTA+$_{all}$",
    "VGTA+$_{00}$",
    "VGTA+$_{00,01}$",
    "VGTA+$_{all}$",
    "3DCNN$_{00}$",
    "3DCNN$_{00,01}$",
    "3DCNN$_{all}$"
]

# EXPERIMENT INFO
start_scenario = 0
num_scenarios = 11


# CRATE GRAPHS
# 1) PARSE files

datas = []
for path in paths:
    datas.append([])

for i in range(num_scenarios):
    for data, dir_path in zip(datas, paths):
        file_path = dir_path + "scenario" + str(i).zfill(2) + ".txt"
        inst = Data(file_path)
        data.append(inst)

# ALLINEATE THE DATA (with sec and nsec fields)

min_time = []
index_of_min_time = []

print()
print()
print()


if len(datas) > 200:
    for i in range(num_scenarios):
        min_time.append(gettime(datas[0][i].sec[0], datas[0][i].nsec[0]))
        index_of_min_time.append(0)

    ## PRINT min times before parsing
    # print(min_time)
    # print(index_of_min_time)
    # print()

    for data_idx, data in enumerate(datas):
        for i in range(1, len(data)):
            if len(data[i].sec) > 0:
                t = gettime(data[i].sec[0], data[i].nsec[0])
                if t < min_time[i]:
                    min_time[i] = t
                    index_of_min_time[i] = data_idx

    ## PRINT min times after parsing
    # print(min_time)
    # print(index_of_min_time)
    # print()

    for data_idx, data in enumerate(datas):
        for i in range(1, len(data)):
            if len(data[i].sec) > 0:
                t = gettime(data[i].sec[0], data[i].nsec[0])
                if t > min_time[i]:
                    header_s = []
                    header_n = []
                    cont = 0
                    while cont < 50:
                        st = gettime(datas[index_of_min_time[i]][i].sec[cont], datas[index_of_min_time[i]][i].nsec[cont])
                        if st < t:
                            header_s.append(datas[index_of_min_time[i]][i].sec[cont])
                            header_n.append(datas[index_of_min_time[i]][i].nsec[cont])
                        else:
                            break
                        cont += 1
                    data[i].fillWithTimeHeader(header_s, header_n)
                    # print("data of ", data_idx, " in scenario ", i, "augmented with ", header_s, header_n)

    ## VERIFY ALIGNMENT
    # for i in range(0, num_scenarios)):
    #     for data_idx, data in enumerate(datas):
    #         print(gettime(data[i].sec[0], data[i].nsec[0]), "   ", end="")
    #     print (" // ", end="")
    #     for data_idx, data in enumerate(datas):
    #         print(gettime(data[i].sec[1], data[i].nsec[1]), "   ", end="")
    #     print (" // ", end="")
    #     for data_idx, data in enumerate(datas):
    #         print(gettime(data[i].sec[2], data[i].nsec[2]), "   ", end="")
    #     print()
    #     print()



# exit(0)

# 2) FIND MAX NUM OF FRAMES (for visualization)
list_len = []
for i in range(num_scenarios - start_scenario):
    for data in datas:
        list_len.append(len(data[i].accuracies))
max_len = max(list_len)

for i in range(num_scenarios):
    for data in datas:
        data[i].fillwithzero(max_len)


################## REDUCE
reduceflag = True
step = 5
selected = [0, 1, 9, 10]
# selected = list(range(num_scenarios))

selected_datas = [datas[0], datas[1], datas[4], datas[8], datas[13]]
selected_legend = [legend[0], legend[1], legend[4], legend[8], legend[13]]
# ################## REDUCE
# reduceflag = True
# step = 5
# selected = [0, 1, 9, 10]
# # selected = list(range(num_scenarios))
#
# selected_datas = [datas[0], datas[1], datas[4], datas[10], datas[11]]
# selected_legend = [legend[0], legend[1], legend[4], legend[10], legend[11]]


if reduceflag:
    frames = list(range(math.ceil(max_len / step)))
else:
    frames = list(range(max_len))

# 3) PLOT

BIG_FONTSIZE = 20
SMALL_FONTSIZE = 10

plt.rc('font', size=SMALL_FONTSIZE)
# plt.margins(y=0)

fig, axs = plt.subplots(len(selected))
fig.set_size_inches(14.5, 8)

# for i in range(num_scenarios):
for cont, i in enumerate(selected):
    axs[cont].set_ylim([0, 1])
    avgs = []
    ylabel = ""
    for idx, data in enumerate(selected_datas):

        graph = data[i].accuracies

        if reduceflag:
            graph = getreduced(graph, step)

        nonzero = [e for i, e in enumerate(graph) if e != 0]
        if (len(nonzero)==0):
            avg= 0
        else:
            avg = sum(nonzero)/len(nonzero)
        avgs.append(avg)

        axs[cont].plot(frames, graph, label=selected_legend[idx])

    axs[cont].set_ylabel("scen_"+str(selected[cont]).zfill(2), rotation=90, labelpad=0)

handles, labels = axs[0].get_legend_handles_labels()
fig.legend(handles, labels, loc='upper left')

plt.rc('font', size=BIG_FONTSIZE)
fig.text(0.04, 0.5, 'Accuracy', va='center', rotation='vertical')

fig.text(0.4, 0.05, 'Frame number / 5', va='center', rotation='horizontal')
plt.rc('font', size=SMALL_FONTSIZE)

# fig.tight_layout()
plt.show()
fig.savefig('/home/fusy/Desktop/accuracy_2.png', dpi=300)

# exit(0)

fig, axs = plt.subplots(len(selected))
fig.set_size_inches(14.5, 8)

# for i in range(num_scenarios):
for cont, i in enumerate(selected):
    axs[cont].set_ylim([0, 1])
    avgs = []
    ylabel = ""
    for idx, data in enumerate(selected_datas):

        graph = data[i].iou

        if reduceflag:
            graph = getreduced(graph, step)

        nonzero = [e for i, e in enumerate(graph) if e != 0]
        if (len(nonzero)==0):
            avg= 0
        else:
            avg = sum(nonzero)/len(nonzero)
        avgs.append(avg)

        axs[cont].plot(frames, graph, label=selected_legend[idx])

    axs[cont].set_ylabel("scen_"+str(selected[cont]).zfill(2), rotation=90, labelpad=0)

handles, labels = axs[0].get_legend_handles_labels()
fig.legend(handles, labels, loc='upper left')

plt.rc('font', size=BIG_FONTSIZE)
fig.text(0.04, 0.4, 'IoU', va='center', rotation='vertical')
fig.text(0.4, 0.05, 'Frame number / 5', va='center', rotation='horizontal')
plt.rc('font', size=SMALL_FONTSIZE)

plt.show()
fig.savefig('/home/fusy/Desktop/iou_2.png', dpi=300)


fig, axs = plt.subplots(len(selected))
fig.set_size_inches(14.5, 8)

# for i in range(num_scenarios):
for cont, i in enumerate(selected):
    axs[cont].set_ylim([0, 1])
    avgs = []
    ylabel = ""
    for idx, data in enumerate(selected_datas):

        graph = data[i].f1score

        if reduceflag:
            graph = getreduced(graph, step)

        nonzero = [e for i, e in enumerate(graph) if e != 0]
        if (len(nonzero)==0):
            avg= 0
        else:
            avg = sum(nonzero)/len(nonzero)
        avgs.append(avg)

        axs[cont].plot(frames, graph, label=selected_legend[idx])

    axs[cont].set_ylabel("scen_"+str(selected[cont]).zfill(2), rotation=90, labelpad=0)

handles, labels = axs[0].get_legend_handles_labels()
fig.legend(handles, labels, loc='upper left')

plt.rc('font', size=BIG_FONTSIZE)
fig.text(0.04, 0.4, 'F1 Score', va='center', rotation='vertical')
fig.text(0.4, 0.05, 'Frame number / 5', va='center', rotation='horizontal')

plt.rc('font', size=SMALL_FONTSIZE)


plt.show()
fig.savefig('/home/fusy/Desktop/f1score_2.png', dpi=300)


# exit(0)




for i in range(num_scenarios):
    output = open("2_table_scen" + str(i).zfill(2) + ".txt", "w")
    output.write("\\begin{table}[t]\n")
    output.write("\\begin{center}\n")
    output.write("\\begin{tabular}{||l|l|l|l|l|l|l|l||}\n")
    output.write("\\hline\n")
    output.write("\\textbf{Experiment} & \\textbf{Acc} & \\textbf{IoU} & \\textbf{F1} & \\textbf{TPR} & \\textbf{TNR} & \\textbf{FPR} & \\textbf{FNR} \\\\ [0.5ex]\n")
    output.write("\\hline\n")

    best = [0, 0, 0, 0, 0, 2, 2]
    mode = ["max", "max", "max", "max", "max", "min", "min",]

    # find bests
    for idx, data in enumerate(datas):
        elems = [
            data[i].accuracies,
            data[i].iou,
            data[i].f1score,
            data[i].TPR,
            data[i].TNR,
            data[i].FPR,
            data[i].FNR
        ]
        for elem_idx, elem in enumerate(elems):
            avg = 0
            nonzero = [e for i, e in enumerate(elem) if e != 0]
            if len(nonzero) > 0:
                avg = sum(nonzero)/len(nonzero)
            if (mode[elem_idx] == "max"):
                if avg > best[elem_idx]:
                    best[elem_idx] = avg
            else:
                if avg < best[elem_idx]:
                    best[elem_idx] = avg

    for idx, data in enumerate(datas):
        elems = [
            data[i].accuracies,
            data[i].iou,
            data[i].f1score,
            data[i].TPR,
            data[i].TNR,
            data[i].FPR,
            data[i].FNR
        ]

        output.write(legend[idx])

        for elem_idx, elem in enumerate(elems):
            avg = 0
            nonzero = [e for i, e in enumerate(elem) if e != 0]
            if len(nonzero) > 0:
                avg = sum(nonzero)/len(nonzero)

            if (avg == best[elem_idx]):
                output.write((" & \\textbf{" + "{:.1f}".format(avg*100)).replace('.', ',') + "}")
            else:
                output.write((" & " + "{:.1f}".format(avg*100)).replace('.', ','))
        output.write(" \\\\ [0.5ex]\n")

    output.write("\\hline\n")
    output.write("\\end{tabular}\n")
    output.write("\\caption{Results on SemanticKITTI Scenario" + str(i).zfill(2) + " (all values are percentages)}\n")
    output.write("\\label{table:results}\n")
    output.write("\\end{center}\n")
    output.write("\\end{table}\n")
    # output.close()

    output.write("\n\n\n")




    output.write("\\begin{table}[t]\n")
    output.write("\\begin{center}\n")
    output.write("\\begin{tabular}{||l|l|l|l|l|l|l|l||}\n")
    output.write("\\hline\n")
    output.write("\\textbf{Experiment} & \\textbf{Acc} & \\textbf{IoU} & \\textbf{F1} & \\textbf{TPR} & \\textbf{TNR} & \\textbf{FPR} & \\textbf{FNR} \\\\ [0.5ex]\n")
    output.write("\\hline\n")



    ######


    for idx, data in enumerate(datas[:2]):
        elems = [
            data[i].accuracies,
            data[i].iou,
            data[i].f1score,
            data[i].TPR,
            data[i].TNR,
            data[i].FPR,
            data[i].FNR
        ]
        for elem_idx, elem in enumerate(elems):
            avg = 0
            nonzero = [e for i, e in enumerate(elem) if e != 0]
            if len(nonzero) > 0:
                avg = sum(nonzero)/len(nonzero)
            if (mode[elem_idx] == "max"):
                if avg > best[elem_idx]:
                    best[elem_idx] = avg
            else:
                if avg < best[elem_idx]:
                    best[elem_idx] = avg

    for idx, data in enumerate(datas[:2]):
        elems = [
            data[i].accuracies,
            data[i].iou,
            data[i].f1score,
            data[i].TPR,
            data[i].TNR,
            data[i].FPR,
            data[i].FNR
        ]

        output.write(legend[idx])

        for elem_idx, elem in enumerate(elems):
            avg = 0
            nonzero = [e for i, e in enumerate(elem) if e != 0]
            if len(nonzero) > 0:
                avg = sum(nonzero)/len(nonzero)

            if (avg == best[elem_idx]):
                output.write((" & \\textbf{" + "{:.1f}".format(avg*100)).replace('.', ',') + "}")
            else:
                output.write((" & " + "{:.1f}".format(avg*100)).replace('.', ','))
        output.write(" \\\\ [0.5ex]\n")

    output.write("\\hline\n")


    ########


    groups = [
        [2, 5, 8],
        [3, 6, 9],
        [4, 7, 10],
        [11, 12, 13]
    ]

    for group in groups:
        sub = []
        for idx in group:
            sub.append(datas[idx])

        best = [0, 0, 0, 0, 0, 2, 2]
        mode = ["max", "max", "max", "max", "max", "min", "min",]

        # find bests for subgroup
        for (idx, data) in zip(group, sub):
            elems = [
                data[i].accuracies,
                data[i].iou,
                data[i].f1score,
                data[i].TPR,
                data[i].TNR,
                data[i].FPR,
                data[i].FNR
            ]
            for elem_idx, elem in enumerate(elems):
                avg = 0
                nonzero = [e for i, e in enumerate(elem) if e != 0]
                if len(nonzero) > 0:
                    avg = sum(nonzero)/len(nonzero)
                if (mode[elem_idx] == "max"):
                    if avg > best[elem_idx]:
                        best[elem_idx] = avg
                else:
                    if avg < best[elem_idx]:
                        best[elem_idx] = avg


        for (idx, data) in zip(group, sub):
            elems = [
                data[i].accuracies,
                data[i].iou,
                data[i].f1score,
                data[i].TPR,
                data[i].TNR,
                data[i].FPR,
                data[i].FNR
            ]

            output.write(legend[idx])

            for elem_idx, elem in enumerate(elems):
                avg = 0
                nonzero = [e for i, e in enumerate(elem) if e != 0]
                if len(nonzero) > 0:
                    avg = sum(nonzero)/len(nonzero)

                if (avg == best[elem_idx]):
                    output.write((" & \\textbf{" + "{:.1f}".format(avg*100)).replace('.', ',') + "}")
                else:
                    output.write((" & " + "{:.1f}".format(avg*100)).replace('.', ','))
            output.write(" \\\\ [0.5ex]\n")

        output.write("\\hline\n")


    # output.write("\\hline\n")
    output.write("\\end{tabular}\n")
    output.write("\\caption{Results on SemanticKITTI Scenario" + str(i).zfill(2) + " (all values are percentages)}\n")
    output.write("\\label{table:results}\n")
    output.write("\\end{center}\n")
    output.write("\\end{table}\n")




output = open("2_table_TOT.txt", "w")

output.write("\\begin{table}[t]\n")
output.write("\\begin{center}\n")
output.write("\\begin{tabular}{||l|l|l|l|l|l|l|l||}\n")
output.write("\\hline\n")
output.write("\\textbf{Experiment} & \\textbf{Acc} & \\textbf{IoU} & \\textbf{F1} & \\textbf{TPR} & \\textbf{TNR} & \\textbf{FPR} & \\textbf{FNR} \\\\ [0.5ex]\n")
output.write("\\hline\n")
num_scenarios = 11

# 2) FIND avg metrics
total = []

for data in datas:
    total.append([0, 0, 0, 0, 0, 0, 0])


for i in range(num_scenarios):
    for data_idx, data in enumerate(datas):
        elems = [
            data[i].accuracies,
            data[i].iou,
            data[i].f1score,
            data[i].TPR,
            data[i].TNR,
            data[i].FPR,
            data[i].FNR
        ]

        for elem_idx, elem in enumerate(elems):
            avg = 0
            nonzero = [e for i, e in enumerate(elem) if e != 0]
            if len(nonzero) > 0:
                avg = sum(nonzero)/len(nonzero)
            total[data_idx][elem_idx] += avg

elems = [
    total[:][0],
    total[:][1],
    total[:][2],
    total[:][3],
    total[:][4],
    total[:][5],
    total[:][6]
]

## average!
for data_idx, data in enumerate(datas):
    for elem_idx, elem in enumerate(elems):
        total[data_idx][elem_idx] /= num_scenarios


for idx, data in enumerate(total):
    print(legend[idx], end=":\t")
    for metr in data:
        print(metr, end=  ",\t")
    print()


best = [0, 0, 0, 0, 0, 100000000000, 1000000000000]
mode = ["max", "max", "max", "max", "max", "min", "min"]

for idx, data in enumerate(datas):

    for elem_idx, elem in enumerate(elems):
        avg = total[idx][elem_idx]
        if (mode[elem_idx] == "max"):
            if avg > best[elem_idx]:
                best[elem_idx] = avg
        else:
            if avg < best[elem_idx]:
                best[elem_idx] = avg

print(best)



for idx, data in enumerate(datas[:2]):

    output.write(legend[idx])

    for elem_idx, elem in enumerate(elems):
        avg = total[idx][elem_idx]
        if (avg == best[elem_idx]):
            output.write((" & \\textbf{" + "{:.1f}".format(total[idx][elem_idx]*100)).replace('.', ',') + "}")
        else:
            output.write((" & " + "{:.1f}".format(total[idx][elem_idx]*100)).replace('.', ','))
    output.write(" \\\\ [0.5ex]\n")

output.write("\\hline\n")


groups = [
    [2, 5, 8],
    [3, 6, 9],
    [4, 7, 10],
    [11, 12, 13]
]

for group in groups:
    sub = []
    for idx in group:
        sub.append(datas[idx])

    local_best = [0, 0, 0, 0, 0, 100000000000, 1000000000000]
    for (data_idx, data) in zip(group, sub):

        for elem_idx, elem in enumerate(elems):
            avg = total[data_idx][elem_idx]
            if (mode[elem_idx] == "max"):
                if avg > local_best[elem_idx]:
                    local_best[elem_idx] = avg
            else:
                if avg < local_best[elem_idx]:
                    local_best[elem_idx] = avg

    print(local_best)


    for (data_idx, data) in zip(group, sub):
        output.write(legend[data_idx])

        for elem_idx, elem in enumerate(elems):
            avg = total[data_idx][elem_idx]
            if (avg == local_best[elem_idx]):
                output.write((" & \\textbf{" + "{:.1f}".format(total[data_idx][elem_idx]*100)).replace('.', ',') + "}")
            else:
                output.write((" & " + "{:.1f}".format(total[data_idx][elem_idx]*100)).replace('.', ','))
        output.write(" \\\\ [0.5ex]\n")
    output.write("\\hline\n")


# output.write("\\hline\n")
output.write("\\end{tabular}\n")
output.write("\\caption{Average of the results among all SemanticKITTI scenarios (all values are percentages.)}\n")
output.write("\\label{table:results_all}\n")
output.write("\\end{center}\n")
output.write("\\end{table}\n")




output.close()
exit(0)















output = open("stats.csv", "w")

output.write("; accuracy; iou; f1score; TPR; TNR; FPR; FNR\n")

for i in range(num_scenarios):
    #print("SCENARIO #" + str(i).zfill(2))
    output.write("SCENARIO #" + str(i).zfill(2) + "\n")
    for idx, data in enumerate(datas):
        #print(" --> " + legend[idx] + ": ", end="")
        elems = [
            data[i].accuracies,
            data[i].iou,
            data[i].f1score,
            data[i].TPR,
            data[i].TNR,
            data[i].FPR,
            data[i].FNR
        ]

        output.write(legend[idx] + "; ")

        for elem_idx, elem in enumerate(elems):
            avg = 0
            nonzero = [e for i, e in enumerate(elem) if e != 0]
            if len(nonzero) > 0:
                avg = sum(nonzero)/len(nonzero)
            #print(elems_names[elem_idx] + ": " + "{:.2f}".format(avg), end=", ")
            output.write(("{:.1f}".format(avg*100)).replace('.', ',') + "; ")
        # print()
        output.write("\n")

output.close()
