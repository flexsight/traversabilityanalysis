import subprocess
import time
import rospy
import roslaunch
import os


def createDir(path):
    try:
        if not os.path.exists(path):
            os.mkdir(path)
    except OSError as e:
        print(e)
        exit(1)


# EXPERIMENT INFO
num_scenarios = 2
launch_file_path = '/home/fusy/repos/my_TraversabilityAnalysis/traversability_mapping/src/TRAV-ANALYSIS/launch/produceFeature_memory_colored_loam_gridSearch.launch'
train_launch_file_path = 'trainModel_gridSearch.launch'

# EXPERIMENT DIRECTORY INFO
dir_cwd = "/home/fusy/.ros/trav_analysis/data/"
dir_name = "exp0"
dir_path = dir_cwd + dir_name + "/"
createDir(dir_path)

# MY EXPERIMENT DIRECTORY INFO
dir_path_mine = dir_path + "train_models_multiscan_colored_loam_curvity/"
createDir(dir_path_mine)

# BAG INFO
bag_cwd = "/home/fusy/tests/semantikitti2bag"
bag_name_base = "semantickitti_sequence"
bag_time_start = 0
bag_duration = 5
bag_rate = 0.3

# GENERAL DATA INFO
data_path = "/home/fusy/.ros/trav_analysis/data/multiscan_colored_loam/"


params = ['rosbag', 'play', bag_name_base, "--clock", "-s " + str(bag_time_start), "-u " + str(bag_duration),
          "-r " + str(bag_rate)]

rospy.init_node('session_node', anonymous=True)
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
roslaunch.configure_logging(uuid)

curvityBucketsNum_singlescan_params = [50, 60, 70, 80, 90, 100,  110, 120, 130, 140, 150, 160, 170, 180, 190, 200]

for curvityBucketsNum_singlescan in curvityBucketsNum_singlescan_params:

    print("--------> STARTED iterarion with single_scan:", curvityBucketsNum_singlescan)


    # PRODUCE EXTRACTS

    # for i in range(num_scenarios):
    #     remap_dataPath       = "dataPath:=extract_" + str(i).zfill(2) + ".txt"
    #     remap_singlescan     = 'curvityBucketsNum_singlescan:='        + str(curvityBucketsNum_singlescan)
    #     # remap_normconfigs    = 'normalizationConfigPath:=norm_params_' + str(curvityBucketsNum_singlescan)  + '.txt'
    #
    #     cli_args = [launch_file_path, remap_dataPath, remap_singlescan]
    #     roslaunch_args = cli_args[1:]
    #     roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
    #
    #     launch = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    #     launch.start()
    #     rospy.loginfo("started")
    #
    #     bag_name = bag_name_base + str(i).zfill(2) + ".bag"
    #     params[2] = bag_name
    #     player_call = subprocess.Popen(params, cwd=bag_cwd)
    #
    #     time.sleep(bag_duration/bag_rate)
    #
    #     launch.shutdown()
    #
    #     player_call.kill()
    #
    #     time.sleep(3)
    #
    # # MERGE EXTRACTS
    # merge_params = ['python3', '/home/fusy/.ros/trav_analysis/data/multiscan_colored_loam/merge.py', str(15000000), "merged_" + str(curvityBucketsNum_singlescan) + ".txt"]
    #
    # merge_player_call = subprocess.Popen(merge_params, cwd=data_path)
    #
    # time.sleep(2)
    # merge_player_call.kill()

    # TRAIN MODEL
    # remap_dataPath       = "dataPath:=merged_" + str(curvityBucketsNum_singlescan) + ".txt"
    remap_dataPath       = "dataPath:=extract_00.txt"
    remap_singlescan     = 'curvityBucketsNum_singlescan:='        + str(curvityBucketsNum_singlescan)
    remap_rbfpath        = 'rbfSVMPath:=rbf_svm_'                  + str(curvityBucketsNum_singlescan)  + '.yml'
    remap_normconfigs    = 'normalizationConfigPath:=norm_params_' + str(curvityBucketsNum_singlescan)  + '.txt'

    train_process_params = ['roslaunch', 'trav_analysis', train_launch_file_path, remap_dataPath, remap_singlescan, remap_rbfpath, remap_normconfigs]

    train_player_call = subprocess.Popen(train_process_params)

    train_player_call.wait()

    train_player_call.kill()

    time.sleep(3)