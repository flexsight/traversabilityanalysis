import subprocess
import time
import rospy
import roslaunch
import os

import matplotlib.pyplot as plt
import numpy as np


def createDir(path):
    try:
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
    except OSError as e:
        print(e)
        exit(1)

# EXPERIMENT INFO
num_scenarios = 11
launch_file_path = '/home/fusy/repos/my_TraversabilityAnalysis/traversability_mapping/src/TRAV-ANALYSIS/launch/produceFeature_memory_colored_loam.launch'

# EXPERIMENT DIRECTORY INFO
# dir_cwd = "/home/fusy/.ros/trav_analysis/data/"
# dir_name = "exp_thesis"
# dir_path = dir_cwd + dir_name + "/"
# createDir(dir_path)

# # MY EXPERIMENT DIRECTORY INFO
# dir_path_mine = dir_path + "trav_analysis_VGTA+/"
# createDir(dir_path_mine)

# BAG INFO
bag_cwd = "/home/fusy/tests/semantikitti2bag"
bag_name_base = "semantickitti_sequence"
bag_time_start = 0
bag_duration = 5
bag_rate = 0.4


params = ['rosbag', 'play', bag_name_base, "--clock", "-q", "-u " + str(bag_duration),
          "-r " + str(bag_rate)]

rospy.init_node('session_node', anonymous=True)
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
roslaunch.configure_logging(uuid)

for i in range(num_scenarios):
    bag_name = bag_name_base + str(i).zfill(2) + ".bag"
    params[2] = bag_name

    dataPath = "extract_" + str(i).zfill(2) + ".txt"

    cli_args = [launch_file_path, 'dataPath:='+dataPath]
    roslaunch_args = cli_args[1:]
    roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]

    launch = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    launch.start()
    rospy.loginfo("started")

    player_call = subprocess.Popen(params, cwd=bag_cwd)

    time.sleep(bag_duration/bag_rate)
    time.sleep(2)

    launch.shutdown()

    time.sleep(2)

    player_call.kill()
