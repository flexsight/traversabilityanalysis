import matplotlib.pyplot as plt
from matplotlib.legend import _get_legend_handles_labels


class Data:

    def fillwithzero(self, maxlen):
        while len(self.frames) < maxlen:
            self.frames.append(0)
            self.sec.append(0)
            self.nsec.append(0)
            self.accuracies.append(0)
            self.fake_accuracy.append(0)
            self.miou.append(0)
            self.f1score.append(0)
            self.FPR.append(0)
            self.TPR.append(0)
            self.FNR.append(0)
            self.TNR.append(0)
            self.simulation_duration.append(0)

    def __init__(self, filepath):
        self.frames = []
        self.sec = []
        self.nsec = []
        self.accuracies = []
        self.fake_accuracy = []
        self.miou = []
        self.f1score = []
        self.FPR = []
        self.TPR = []
        self.FNR = []
        self.TNR = []
        self.simulation_duration = []
        print(filepath, end="")
        try:
            file = open(filepath)
        except:
            print(" not found!")
            return
        print(" found :)")
        for line in file:
            line = line.split(" ")
            linelen = len(line)
            self.frames.append(float(line[0]))
            self.sec.append(float(line[1]))
            self.nsec.append(float(line[2]))
            self.accuracies.append(float(line[3]))
            self.fake_accuracy.append(float(line[4]))
            self.miou.append(float(line[5]))
            self.f1score.append(float(line[6]))
            self.FPR.append(float(line[7]))
            self.TPR.append(float(line[8]))
            self.FNR.append(float(line[9]))
            self.TNR.append(float(line[10]))
            if linelen > 11:
                self.simulation_duration.append(float(line[11])/1000)
            else:
                self.simulation_duration.append(0)
        file.close()


# EXPERIMENT DIRECTORY INFO
dir_cwd = "/home/fusy/.ros/trav_analysis/data/exp1/"

# dir_path_original = dir_cwd + "original_trav/"
# dir_path_multiscancolored = dir_cwd + "trav_analysis_geom_00/"
# dir_path_multiscancolored_00_01 = dir_cwd + "trav_analysis_00_01/"
# dir_path_multiscancolored_00_01_geom = dir_cwd + "trav_analysis_00_01_geom/"
# dir_path_curvity = dir_cwd + "trav_analysis_00_01_curvity/"
#
# paths = [
#     # dir_path_original,
#     dir_path_multiscancolored,
#     # dir_path_multiscancolored_00_01,
#     # dir_path_multiscancolored_00_01_geom,
#     # dir_path_curvity
# ]
#
# legend = [
#     "original",
#     "multiscan_00",
#     "multiscan_00_01",
#     "multiscan_00_01_geom",
#     "curvity"
# ]

dir_path_original = dir_cwd + "original_trav/"
dir_path_geom_00 = dir_cwd + "trav_analysis_geom_00/"
dir_path_geom_00_01 = dir_cwd + "trav_analysis_geom_00_01/"
dir_path_geom_00_01_allscen = dir_cwd + "trav_analysis_geom_all_features/"
# dir_path_allfeat_00 = dir_cwd + "trav_analysis_geom_00/"
# dir_path_allfeat_00_01 = dir_cwd + "trav_analysis_geom_00_01/"
# dir_path_allfeat_allscen = dir_cwd + "trav_analysis_00_01_geom/"
dir_path_curvity = dir_cwd + "trav_analysis_00_01_curvity/"

paths = [
    # dir_path_original,
    dir_path_geom_00,
    dir_path_geom_00_01,
    dir_path_geom_00_01_allscen,
    # dir_path_allfeat_00
    # dir_path_allfeat_allscen
    # dir_path_curvity
]

legend = [
    # "original",
    "geom_00",
    "geom_00_01",
    "geom_allscen",
    "allfeats_00",
    "allfeats_00_01",
    "allfeats_allscen",
]

# EXPERIMENT INFO
start_scenario = 0
num_scenarios = 11


# CRATE GRAPHS
# 1) PARSE files

datas = []
for path in paths:
    datas.append([])

for i in range(num_scenarios):
    for data, dir_path in zip(datas, paths):
        file_path = dir_path + "scenario" + str(i).zfill(2) + ".txt"
        inst = Data(file_path)
        data.append(inst)

# 2) FIND MAX NUM OF FRAMES (for visualization)
list_len = []
for i in range(num_scenarios - start_scenario):
    for data in datas:
        list_len.append(len(data[i].accuracies))
max_len = max(list_len)

for i in range(num_scenarios):
    for data in datas:
        data[i].fillwithzero(max_len)

# 3) PLOT
frames = list(range(max_len))

fig, axs = plt.subplots(num_scenarios)
fig.suptitle('Accuracies')


if num_scenarios > 1:

    for i in range(num_scenarios):
        axs[i].set_ylim([0, 1])
        avgs = []
        ylabel = ""
        for idx, data in enumerate(datas):
            nonzero = [e for i, e in enumerate(data[i].accuracies) if e != 0]
            if (len(nonzero)==0):
                avg= 0
            else:
                avg = sum(nonzero)/len(nonzero)
            avgs.append(avg)
            ylabel += legend[idx] + "_avg_acc: " + "{:.2f}".format(avg) + "\n"
            axs[i].plot(frames, data[i].accuracies, label=legend[idx])

        axs[i].set_ylabel(ylabel[:-1], rotation=0, labelpad=85)

# else:
#     fig, axs = plt.subplots(num_scenarios)
#     axs.set_ylim([0, 1])
#     axs.plot(frames, original_data[0].accuracies)
#     axs.plot(frames, multiscancolored_data[0].accuracies)
handles, labels = axs[0].get_legend_handles_labels()
fig.legend(handles, labels)
plt.show()
