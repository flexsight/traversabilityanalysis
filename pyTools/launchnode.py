import roslaunch
import rospy
import time

import sys

time_duration = int(sys.argv[1])

rospy.init_node('session_node', anonymous=True)
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
roslaunch.configure_logging(uuid)
launch = roslaunch.parent.ROSLaunchParent(uuid, ["/home/fusy/repos/my_TraversabilityAnalysis/traversability_mapping/src/TRAV-ANALYSIS/launch/onlineSystem_memory_colored_loam.launch trav"])
launch.start()
rospy.loginfo("started")

rospy.sleep(time_duration)
# time_duration seconds later
launch.shutdown()