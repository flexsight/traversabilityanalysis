import subprocess
import time
import rospy
import roslaunch
import os

import matplotlib.pyplot as plt
import numpy as np


def createDir(path):
    try:
        if not os.path.exists(path):
            os.mkdir(path)
    except OSError as e:
        print(e)
        exit(1)

# EXPERIMENT INFO
num_scenarios = 11
launch_file_path = '/home/fusy/repos/my_TraversabilityAnalysis/traversability_mapping/src/TRAV-ANALYSIS/launch/onlineSystem_memory_colored_loam_map.launch'

# EXPERIMENT DIRECTORY INFO
dir_cwd = "/home/fusy/.ros/trav_analysis/data/"
dir_name = "exp_thesis"
dir_path = dir_cwd + dir_name + "/"
createDir(dir_path)

# MY EXPERIMENT DIRECTORY INFO
dir_path_mine = dir_path + "VGTA+_00/"
createDir(dir_path_mine)

# BAG INFO
bag_cwd = "/home/fusy/tests/semantikitti2bag"
bag_name_base = "semantickitti_sequence"
bag_time_start = 5
bag_duration = 50
bag_rate = 0.45


params = ['rosbag', 'play', bag_name_base, "--clock", "-q", "-s " + str(bag_time_start), "-u " + str(bag_duration),
          "-r " + str(bag_rate)]

rospy.init_node('session_node', anonymous=True)
uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
roslaunch.configure_logging(uuid)

for i in range(2, num_scenarios):
# for i in [0, 1, 9, 10]:

    dir_final_path = dir_path_mine + "scenario" + str(i).zfill(2) + ".txt"

    cli_args = [launch_file_path, 'outStatsPath:='+dir_final_path]
    roslaunch_args = cli_args[1:]
    roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]

    launch = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
    launch.start()
    rospy.loginfo("started")

    bag_name = bag_name_base + str(i).zfill(2) + ".bag"
    params[2] = bag_name
    player_call = subprocess.Popen(params, cwd=bag_cwd)

    time.sleep(bag_duration/bag_rate)

    launch.shutdown()

    player_call.kill()